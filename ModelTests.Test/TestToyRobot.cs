﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Toy;

namespace ModelTests.Test
{
    [TestClass]
    public class TestToyRobot
    {
        /// <summary>
        /// Test toy turn left
        /// </summary>
        [TestMethod]
        public void ToyActionInvoked_TurnToyLeft_ToyDirectionChanges()
        {
            // arrange
            var robot = new ToyRobot { Direction = Direction.ENUMWEST, Position = new Position(2, 2) };

            // act
            robot.RotateLeft();

            // assert
            Assert.AreEqual(Direction.ENUMSOUTH, robot.Direction);
        }

        /// <summary>
        /// Test toy turn right
        /// </summary>
        [TestMethod]
        public void ToyActionInvoked_TurnToyRight_ToyDirectionChanges()
        {
            // arrange
            var robot = new ToyRobot { Direction = Direction.ENUMWEST, Position = new Position(2, 2) };

            // act
            robot.RotateRight();

            // assert
            Assert.AreEqual(Direction.ENUMNORTH, robot.Direction);
        }


        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void ToyActionInvoked_MoveToy_ToyPositionChanges()
        {
            // arrange
            var robot = new ToyRobot { Direction = Direction.ENUMEAST, Position = new Position(2, 2) };

            // act
            var nextPosition = robot.GetNextPosition();

            // assert
            Assert.AreEqual(3, nextPosition.X);
            Assert.AreEqual(2, nextPosition.Y);
        }
    }
}
