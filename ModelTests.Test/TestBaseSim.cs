﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Factories;
namespace ModelTests.Test
{
    [TestClass]
    public class TestBaseSim
    {
        [TestMethod]
        public void ResourceTextRequested_WithStringName_ReturnsString()
        {
            // arrange
            var thisSim = ToyFactory.CreateToySim(5, 5);
            
            // act
            var result = thisSim.GetResourceText("CoordinatesLabel");

            // assert
            Assert.IsInstanceOfType(result, typeof(string));
        }
    }
}
