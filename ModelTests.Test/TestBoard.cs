﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.ToyBoard;
using Model.Toy;

namespace ModelTests.Test
{
    [TestClass]
    public class TestBoard
    {
        [TestMethod]
        public void BoardPositionCheck_WithInvalidPosition_ReturnsFalse()
        {
            // arrange
            ToyBoard squareBoard = new ToyBoard(5, 5);
            Position position = new Position(6, 6);

            // act
            var result = squareBoard.IsValidPosition(position);

            // assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Test valid positon 
        /// </summary>
        [TestMethod]
        public void BoardPositionCheck_WithValidPosition_ReturnsTrue()
        {
            // arrange
            ToyBoard squareBoard = new ToyBoard(5, 5);
            Position position = new Position(1, 4);

            // act
            var result = squareBoard.IsValidPosition(position);

            // assert
            Assert.IsTrue(result);
        } 
    }
}
