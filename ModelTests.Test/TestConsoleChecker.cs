﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.ConsoleChecker;
using Model.Toy;
using Model.Factories;

namespace ModelTests.Test
{
    [TestClass]
    public class TestConsoleChecker
    {
        /// <summary>
        /// Test valid place command
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithValidCommand_ReturnsEnum()
        {
            // arrange
            var inputParser = new InputParser();
            string[] rawInput = "PLACE".Split(' ');

            // act
            var command = inputParser.ParseCommand(rawInput);

            // assert
            Assert.AreEqual(Command.ENUMPLACE, command);
        }

        /// <summary>
        /// Test an invalid place command
        /// checks the inputarray[0] element only
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithInvalidCommand_ThrowsEx()
        {
            // arrange
            var toySim = ToyFactory.CreateToySim(5, 5);
            var inputParser = new InputParser();
            string[] rawInput = "PLACETOY".Split(' ');
            try
            {
                // act
                inputParser.ParseCommand(rawInput);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                // assert
                Assert.AreEqual(toySim.GetResourceText("CommandErrorMessage"), ex.Message);
            }
        }

        /// <summary>
        /// Test a full place command with valid parameters
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithValidParams_CreatesCommandParameterObject()
        {
            // arrange
            var inputParser = new InputParser();
            string[] rawInput = "PLACE 4,3,WEST".Split(' ');

            // act
            var placeCommandParameter = inputParser.ParseCommandParameter(rawInput);

            // assert
            Assert.AreEqual(4, placeCommandParameter.Position.X);
            Assert.AreEqual(3, placeCommandParameter.Position.Y);
            Assert.AreEqual(Direction.ENUMWEST, placeCommandParameter.Direction);
        }

        /// <summary>
        /// Test a place command with an incomplete parameter
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithInvalidCommandAndParams_ThrowsEx()
            

        {
            // arrange
            var toySim = ToyFactory.CreateToySim(5, 5);
            var inputParser = new InputParser();
            string[] rawInput = "PLACE 3,1".Split(' ');
            try
            {
                // act
                inputParser.ParseCommandParameter(rawInput); 
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                // assert
                Assert.AreEqual(toySim.GetResourceText("CommandIncompleteMessage"), ex.Message);
            }
        }

        /// <summary>
        /// Test a place command with an invalid direction
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithInvalidDirection_ThrowsEx()
        {
            // arrange
            var toySim = ToyFactory.CreateToySim(5, 5);
            var paramParser = new PlaceCommandParameterParser();
            string[] rawInput = "PLACE 2,4,OneDirection".Split(' ');
            try
            {
                // act
                paramParser.ParseParameters(rawInput);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                // assert
                Assert.AreEqual(toySim.GetResourceText("DirectionInvalidMessage"), ex.Message);
            }        
        }

        /// <summary>
        /// Test a place command with an invalid parameter format
        /// </summary>
        [TestMethod]
        public void PlaceCommandEntered_WithInvalidParams_ThrowsEx()
        {
            // arrange
            var toySim = ToyFactory.CreateToySim(5, 5);
            var paramParser = new PlaceCommandParameterParser();
            string[] rawInput = "PLACE 3,3,SOUTH,2".Split(' ');
            try
            {
                // act
                paramParser.ParseParameters(rawInput);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                // assert
                Assert.AreEqual(toySim.GetResourceText("CommandIncompleteMessage"), ex.Message);
            } 
        }
    }
}
