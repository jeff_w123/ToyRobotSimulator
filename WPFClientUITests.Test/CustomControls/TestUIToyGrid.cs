﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows;
using WPFClient.View.Behaviours;

namespace WPFClientVMTests.Test.CustomControls
{
    [TestClass]
    public class TestUIToyGrid
    {
        [TestMethod]
        public void ToyGridComponents_WhenMaxValue6SelectedSize4_SetsBtnVisibility()
        {
            // arrange
            var testGrid = new ToyGrid();
            var visibleBtns = 0;
            var invisibleBtns = 0;

            // act
            testGrid.MaxGridSize = 6;
            testGrid.SelectedSize = 4;
            foreach (GridButton item in testGrid.Children)
            {
                if (item.Visibility == Visibility.Visible)
                    visibleBtns ++;
                else { invisibleBtns++; };
            }

            // assert
            Assert.AreEqual(16, visibleBtns);
            Assert.AreEqual(20, invisibleBtns);
         }

        [TestMethod]
        public void ToyGridComponents_WhenMaxValue5SelectedSize3_SetsSizes()
        {
            // arrange
            var testGrid = new ToyGrid();

            // act
            testGrid.MaxGridSize = 5;
            testGrid.SelectedSize = 3;
            var gridColumns = testGrid.ColumnDefinitions;
            var gridRows = testGrid.RowDefinitions;

            // assert
            // just test the crossover indexes
            Assert.IsTrue(gridColumns[2].Width.IsStar);
            Assert.IsTrue(gridColumns[3].Width.IsAuto);
            Assert.IsTrue(gridRows[2].Height.IsStar);
            Assert.IsTrue(gridRows[1].Height.IsAuto);
        }
    }
}