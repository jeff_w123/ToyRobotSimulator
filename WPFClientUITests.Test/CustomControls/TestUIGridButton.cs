﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToySimWCFService;
using WPFClient.View.Behaviours;
using WPFClientUITests.Test.Helpers;

namespace WPFClientVMTests.Test.CustomControls
{
    [TestClass]
    public class TestUIGridButton
    {
        [TestMethod]
        public void GridButtonAppearance_WhenRobotIsNull_ShowsCoordinates()
        {
            // arrange and act
            var btn = new GridButton(2, 3);

            // assert
            Assert.IsNull(btn.GridToyRobot);
            Assert.AreEqual("2 , 3", btn.Content);
        }

        [TestMethod]
        public void GridButtonAppearance_WithRobotCoordsNotMatching_ShowsCoordinates()
        {
            // arrange
            var robot = new SVCToyRobot();
            robot.RobotPosition = new SVCPosition(4, 5);

            // act
            var btn = new GridButton(2, 4);
            btn.GridToyRobot = robot;

            // assert
            Assert.AreEqual("2 , 4", btn.Content);
        }

        [TestMethod]
        public void GridButtonAppearance_WithRobotCoordsMatching_ShowsImage()
        {
            // arrange
            var robot = new SVCToyRobot();
            robot.RobotPosition = new SVCPosition(1, 2);
            robot.RobotDirection = (int)TestWPFDirection.East;

            // act
            var btn = new GridButton(1, 2);
            btn.GridToyRobot = robot;

            // assert
            Assert.AreEqual(typeof(System.Windows.Controls.Image), btn.Content.GetType());
        }

        [TestMethod]
        public void GridButtonAppearance_WithRobotDirectionSetEast_ShowsEastFacing()
        {
            // arrange
            var btn = new GridButton(2, 3);
            var robot = new SVCToyRobot();
            robot.RobotPosition = new SVCPosition(2, 3);
            robot.RobotDirection = (int)TestWPFDirection.East;
            var helper = new ImageRotationHelper("../../Helpers/Images/");

            // act
            btn.GridToyRobot = robot;
            helper.CreateComparisonBitmaps(btn);

            // assert
            Assert.IsTrue(helper.IsCorrectlyFacing(btn, "East"));
        }

        [TestMethod]
        public void GridButtonAppearance_WithRobotDirectionSetNorth_ShowsNorthFacing()
        {
            // arrange
            var btn = new GridButton(6, 7);
            var robot = new SVCToyRobot();
            robot.RobotPosition = new SVCPosition(6, 7);
            robot.RobotDirection = (int)TestWPFDirection.North;
            var helper = new ImageRotationHelper("../../Helpers/Images/");

            // act
            btn.GridToyRobot = robot;
            helper.CreateComparisonBitmaps(btn);

            // assert
            Assert.IsTrue(helper.IsCorrectlyFacing(btn, "North"));
        }
    }
}