﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClientUITests.Test.Helpers;
using WPFClientUITests.Test.WindowObjects;

namespace WPFClientUITests.Test.AutomatedTests
{
    [TestClass]
    public class TestUIAutomations : AutomationBase
    {
        [TestMethod]
        public void ExitActionInvoked_WithExitBtnClick_ClosesApp()
        {
            // arrange
            var runningStatusBeforeBtnClick = WpfApplication.HasExited;

            // act
            Windows.Main.ClickExitButton();
            WpfApplication.Process.WaitForExit();

            // assert
            Assert.IsFalse(runningStatusBeforeBtnClick, "Error, the application should be running!");
            Assert.IsTrue(WpfApplication.HasExited, "Error, the application should have closed!");
        }

        [TestMethod]
        public void ExitActionInvoked_WithENExitEnteredInConsole_ClosesApp()
        {
            // arrange
            var runningStatusBeforeBtnClick = WpfApplication.HasExited;

            // act
            Windows.Main.EnterConsoleText("exit");
            WpfApplication.Process.WaitForExit();

            // assert
            Assert.IsFalse(runningStatusBeforeBtnClick, "Error, the application should be running!");
            Assert.IsTrue(WpfApplication.HasExited, "Error, the application should have closed!");
        }

        [TestMethod]
        public void ConsoleTxtBehavior_WithReportBtnClick_ScrollsToBottom()
        {
            // arrange
            Windows.Main.PlaceRobotUsingBtns(3, 4, TestWPFDirection.North);
            
            // act
            var scrollBarIsTop = Windows.Main.ScrollBarIsTop();
            Windows.Main.ClickReportButton();
            var scrollBarIsBottom = Windows.Main.ScrollBarIsBottom();
            Windows.Main.ClickExitButton();

            // assert
            Assert.IsTrue(scrollBarIsTop, "Error, the scrollbar should be scrolled UP!");
            Assert.IsTrue(scrollBarIsBottom, "Error, the scrollbar should be scrolled down!");
        }

        [TestMethod]
        public void ConsoleTxtBehavior_WithLanguageChange_ScrollsToTop()
        {
            // arrange
            Windows.Main.PlaceRobotUsingBtns(3, 1, TestWPFDirection.North);
            Windows.Main.EnterConsoleText("This invalid text => error response!");
            var scrollBarIsBottom = Windows.Main.ScrollBarIsBottom();

            // act
            Windows.Main.SelectLanguageIndex(0);
            var scrollBarIsTop = Windows.Main.ScrollBarIsTop();
            Windows.Main.ClickExitButton();

            // assert
            Assert.IsTrue(scrollBarIsBottom, "Error, the scrollbar should be scrolled down!");
            Assert.IsTrue(scrollBarIsTop, "Error, the scrollbar should be scrolled UP!");
        }

        [TestMethod]
        public void SliderBehavior_WithValueIncrement_CreatesComboItem()
        {
            // arrange
            Windows.Main.PlaceRobotUsingBtns(4, 3, TestWPFDirection.North);
            var oldPlaceComboXvalue = Windows.Main.GetPlaceXComboMaxValue();

            // act
            Windows.Main.SetSliderValue(6);
            Windows.Main.PlaceRobotUsingBtns(5, 5, TestWPFDirection.East);
            var newPlaceComboXvalue = Windows.Main.GetPlaceXComboMaxValue();
            Windows.Main.ClickExitButton();

            // assert
            Assert.IsTrue(oldPlaceComboXvalue == "4",
               "Error, a new combo box item was not found!");
            Assert.IsTrue(newPlaceComboXvalue == "5",
               "Error, a new combo box item was not found!");
        }

    }
}
