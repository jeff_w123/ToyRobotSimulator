﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Utility;
using WPFClientUITests.Test.WindowObjects;
namespace WPFClientUITests.Test.AutomatedTests
{
    /// <summary>
    /// A base class to provide:
    /// application initialisation ( the WPF launch)
    /// TestInitialize and TestCleanup implementations ( starting and stopping of the WCF service )
    /// </summary>
    
    [TestClass]
    public class AutomationBase
    {
        private Window _wcfConsole;
        protected Application WpfApplication;
        private string wpfPath = Path.GetFullPath(@"..\\..\\..\\ToySimWPFClient\bin\Debug\ToyRobotClient.exe");
        private string wcfPath = Path.GetFullPath(@"..\\..\\..\\ToySimWCFHost\bin\Debug\ToyRobotService.exe");

        public AutomationBase()
        {
            WpfApplication = Application.Launch(wpfPath);
            Windows.Init(WpfApplication);
        }

        [TestInitialize]
        public void StartWCF()
        {
            // start WCF service
            Application wcfApplication = Application.Launch(wcfPath);
            _wcfConsole = Retry.For(
                () => wcfApplication.GetWindows().Find(x => x.Title.
                    Contains(wcfPath)),
                TimeSpan.FromSeconds(2));
        }

        [TestCleanup]
        public void StopWCF()
        {
            // stop WCF service
            _wcfConsole.Enter("Any key entry...");
            _wcfConsole.WaitTill(() => _wcfConsole.IsClosed);
            _wcfConsole = null;
        }
    }
}
