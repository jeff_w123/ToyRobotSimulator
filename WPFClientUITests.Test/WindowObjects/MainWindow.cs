﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using WPFClientUITests.Test.Helpers;

namespace WPFClientUITests.Test.WindowObjects
{
    /// <summary>
    /// A class to provide functions for the unit testing of the WPF MainWindow
    /// </summary>
    public class MainWindow : WindowObject
    {
        private Button ExitBtn 
        {
            get { return Btn("Exit"); } 
        }

        private TextBox ConsoleInput
        {
            get { return TxtBox("TextBox_Input"); }
        }

        private TextBox ConsoleOutput
        {
            get { return TxtBox("TextBox_Output"); }
        }

        private ComboBox PlaceXCombo
        {
            get { return CboBox("ComboBox_X"); }
        }

        private ComboBox PlaceYCombo
        {
            get { return CboBox("ComboBox_Y"); }
        }

        private Button PlaceBtn
        {
            get { return Btn("Place"); }
        }

        private Button ReportBtn
        {
            get { return Btn("Report"); }
        }

        private ComboBox LangCombo
        {
            get { return CboBox("ComboBox_ChangeLanguage"); }
        }

        private Slider ResizeSlider
        {
            get { return ResizeSlider(); }
        }


        // make ctor internal to stop other assemblies creating instances.
        internal MainWindow(Window window) : base (window)
        {
        }

        public void ClickExitButton()
        {
            ExitBtn.Click();
        }

        public void EnterConsoleText(string textToEnter)
        {
            ConsoleInput.Enter(textToEnter);
            ConsoleInput.KeyIn(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
        }

        public void ClickReportButton()
        {
            ReportBtn.Click();
        }
        
        public void PlaceRobotUsingBtns(int x, int y, TestWPFDirection direction)
        {
            PlaceXCombo.Select(x);
            PlaceYCombo.Select(y);
            DirectionBtn(direction).Click();
            PlaceBtn.Click();
        }

        public TextBox GetConsoleOutputTxtBox()
        {
            return ConsoleOutput;
        }

        public bool ScrollBarIsBottom()
        {
            if (ConsoleOutput.ScrollBars.Vertical.Value 
                == ConsoleOutput.ScrollBars.Vertical.MaximumValue)
                return true;
            return false;
        }

        public bool ScrollBarIsTop()
        {
            if (ConsoleOutput.ScrollBars.Vertical.Value
                == ConsoleOutput.ScrollBars.Vertical.MinimumValue)
                return true;
            return false;
        }

        public void SelectLanguageIndex(int langIndex)
        {
            var itemSelected = LangCombo.Item(langIndex);
            LangCombo.Select(itemSelected.Text);
        }

        public void SetSliderValue(int val)
        {
            ResizeSlider.Value = val;
        }

        public string GetPlaceXComboMaxValue()
        {
            var numberOfItems = PlaceXCombo.Items.Count;
            var lastItem =  PlaceXCombo.Item(numberOfItems - 1);
            return lastItem.Text;
        }
    }
}
