﻿using System;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Utility;
namespace WPFClientUITests.Test.WindowObjects
{
    /// <summary>
    /// A static window repository class for all WPF windows
    /// </summary>
    public static class Windows
    {
        private static Application _application;
        
        // prop to get the main (only) app window
        public static MainWindow Main
        {
            get
            {
                Window window = GetWindow("Toy Robot Simulator v2.0 by Jeff Williams");
                return new MainWindow(window);
            }
        }

        // properties from other windows to go here.....
        public static void Init(Application application)
        {
            _application = application;
        }

        private static Window GetWindow(string title)
        {
            return Retry.For(
                () => _application.GetWindows().Find(x => x.Title.
                    Contains(title)), TimeSpan.FromSeconds(2));
        }
    }
}
