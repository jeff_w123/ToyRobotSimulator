﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using WPFClientUITests.Test.Helpers;
namespace WPFClientUITests.Test.WindowObjects
{
    /// <summary>
    /// A class to provide search capability for controls shared in all WPF windows
    /// </summary>
    public class WindowObject
    {
        protected Window _window;

        protected WindowObject(Window window)
        {
            _window = window;
        }

        protected Button Btn(string btnName)
        {
            return _window.Get<Button>(btnName); ;
        }

        protected TextBox TxtBox(string txtBxName)
        {
            return _window.Get<TextBox>(txtBxName); ;
        }

        protected ComboBox CboBox(string cboxName)
        {
            return _window.Get<ComboBox>(cboxName);
        }

        protected Button DirectionBtn(TestWPFDirection direction)
        {
            var btnName = string.Format("ButtonDirection_{0}", direction.ToString());
            return _window.Get<Button>(btnName);
        }

        protected Slider ResizeSlider()
        {
            return _window.Get<Slider>("Slider_ChangeGridSize");
        }

    }
}
