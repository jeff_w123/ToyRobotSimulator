﻿using System.Globalization;

namespace WPFClientUITests.Test.Helper
{
    class TestWPFComboBoxItem
    {
        public int ComboValue { get; set; }
        public string ComboTextValue { get; set; }
        public CultureInfo ComboCultureValue { get; set; }
    }
}
