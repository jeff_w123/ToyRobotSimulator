﻿namespace WPFClientUITests.Test.Helpers
{
    /// <summary>
    /// This class is used in tests to validate a direction
    /// </summary>
    public enum TestWPFDirection
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }
}
