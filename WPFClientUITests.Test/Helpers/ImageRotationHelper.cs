﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using WPFClient.View.Behaviours;
using WPFClient.Utilities;

namespace WPFClientUITests.Test.Helpers
{
    /// <summary>
    /// A class to validate rotated images - saves to file to provide a visual reference.
    /// </summary>
    public class ImageRotationHelper
    {
        public string SavedImagesLocation { get; set; }
        private Bitmap NorthRobotBitmap { get; set; }

        public ImageRotationHelper(string path)
        { SavedImagesLocation = path; }

        // method to create and save rotated bitmaps, assume that embedded bitmap is north facing.
        public void CreateComparisonBitmaps(GridButton btn)
        {
            // empty the saved images folder
            var di = new DirectoryInfo(SavedImagesLocation);
            foreach (FileInfo file in di.GetFiles()) { file.Delete(); }
            
            NorthRobotBitmap = btn.GetStoredBitmap();

            // create expected
            var rotations = new Dictionary<string, RotateFlipType>()
            {
	            {"ExpectedNORTH", RotateFlipType.RotateNoneFlipNone},
	            {"ExpectedEAST", RotateFlipType.Rotate90FlipNone},
	            {"ExpectedSOUTH", RotateFlipType.RotateNoneFlipY},
	            {"ExpectedWEST", RotateFlipType.Rotate270FlipNone}
	        };
            foreach (var pair in rotations) { SaveBitmap(pair.Key, pair.Value); }

            // create actual
            btn.RobotPng.Save(
                SavedImagesLocation
                + "Actual"
                + ((WPFDirection)btn.GridToyRobot.RobotDirection).ToString()
                + ".png", System.Drawing.Imaging.ImageFormat.Png);
        }

        private Bitmap RotateImage(RotateFlipType thisRotation)
        {
            var bmp = new Bitmap(NorthRobotBitmap);
            using (Graphics gfx = Graphics.FromImage(bmp))
            {
                gfx.Clear(System.Drawing.Color.Transparent);
                gfx.DrawImage(NorthRobotBitmap, 0, 0, NorthRobotBitmap.Width, 
                    NorthRobotBitmap.Height);
            }
            bmp.RotateFlip(thisRotation);
            return bmp;
        }

        private void SaveBitmap(string saveName, RotateFlipType thisRotation)
        {
            var fullSavePath = SavedImagesLocation + saveName + ".png";
            var imageToSave = RotateImage(thisRotation);
            imageToSave.Save(fullSavePath, System.Drawing.Imaging.ImageFormat.Png);
        }

        public bool IsCorrectlyFacing(GridButton btn, String expectedDirection)
        {
            var actualFile = SavedImagesLocation
                + "Actual"
                + ((WPFDirection)btn.GridToyRobot.RobotDirection).ToString()
                + ".png";
            var actual = new Bitmap(actualFile);
            var expectedFile = SavedImagesLocation
                + "Expected"
                + expectedDirection
                + ".png";
            var expected = new Bitmap(expectedFile);
            MemoryStream ms = new MemoryStream();
            actual.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            String actualBitmap = Convert.ToBase64String(ms.ToArray());
            ms.Position = 0;

            expected.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            String expectedBitmap = Convert.ToBase64String(ms.ToArray());
            actual.Dispose();
            expected.Dispose();
            if (actualBitmap.Equals(expectedBitmap))
            {
                return true;
            }
            else
            {

                return false;
            }
        }
    }
}
