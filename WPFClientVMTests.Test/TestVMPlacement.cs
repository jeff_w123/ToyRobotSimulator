﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClient.ViewModel;
using ToySimWCFService;
using Moq;

namespace WPFClientVMTests.Test
{
    [TestClass]
    public class TestVMPlacement
    {      
        [TestMethod]
        public void DirectionBtnSelected_NorthClicked_SetsDirectionProperties()
        {
            // arrange and act
            var Model = new ToySimVM();

            // act
            Model.DirectionAction(3);

            // assert
            Assert.IsTrue(Model.DirectionNorthManifest);
            Assert.IsTrue(Model.DirectionEastManifest);
            Assert.IsTrue(Model.DirectionSouthManifest);
            Assert.IsFalse(Model.DirectionWestManifest);
        }

        [TestMethod]
        public void DirectionBtnSelected_EastClicked_SetsDirectionProperties()
        {
            // arrange and act
            var Model = new ToySimVM();

            Model.DirectionAction(1);

            // assert
            Assert.IsTrue(Model.DirectionNorthManifest);
            Assert.IsFalse(Model.DirectionEastManifest);
            Assert.IsTrue(Model.DirectionSouthManifest);
            Assert.IsTrue(Model.DirectionWestManifest);
        }

        [TestMethod]
        public void PlacementBtnCanAction_AllParamsSet_ReturnsTrue()
        {
            // arrange
            var Model = new ToySimVM();
            Model.ComboBoxXValue = 4;
            Model.ComboBoxYValue = 2;
            Model.DirectionSelectedValue = 3;
            
            // act and assert
            Assert.IsTrue(Model.CanActionArePlaceParamsSet(null));
        }

        [TestMethod]
        public void PlacementBtnCanAction_NotAllParamsSet_ReturnsFalse()
        {
            // arrange
            var Model = new ToySimVM();
            Model.ComboBoxXValue = 1;
            Model.ComboBoxYValue = null;
            Model.DirectionSelectedValue = null;

            // act and assert
            Assert.IsFalse(Model.CanActionArePlaceParamsSet(null));
        }
        
        [TestMethod]
        public void ActionBtnsCanAction_ToyRobotSet_ReturnsTrue()
        {
            // arrange
            var Model = new ToySimVM();
            Model.ToyRobotSim = new SVCToyRobot();

            // act and assert
            Assert.IsTrue(Model.CanActionIsRobotPlaced(null));
        }

        [TestMethod]
        public void ActionBtnsCanAction_ToyRobotNotSet_ReturnsFalse()
        {
            // arrange
            var Model = new ToySimVM();

            // act and assert
            Assert.IsFalse(Model.CanActionIsRobotPlaced(null));
        }

        private ToySimVM CreateMock1()
        {
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());
            return Model;
        }

        [TestMethod]
        public void PlaceBtnAction_WithPlacementParams_CreatesToyRobot()
        {
            //arrange
            var Model = CreateMock1();
            Model.ComboBoxXValue = 3;
            Model.ComboBoxYValue = 2;
            Model.DirectionSelectedValue = 1;

            // act
            Model.PlaceBtnAction(null);

            // assert
            Assert.AreEqual(Model.ToyRobotSim.RobotPosition.X, 3);
            Assert.AreEqual(Model.ToyRobotSim.RobotPosition.Y, 2);
            Assert.AreEqual(Model.ToyRobotSim.RobotDirection, 1);
        }
    }
}
