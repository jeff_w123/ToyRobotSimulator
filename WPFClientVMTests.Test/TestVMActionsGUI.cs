﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClient.ViewModel;
using Moq;
using ToySimWCFService;

namespace WPFClientVMTests.Test
{
    [TestClass]
    public class TestVMActionsGUI
    {
        [TestMethod]
        public void UserClicksButton_LeftBtnClicked_RotateLeftShouldBeInvoked()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());

            // act and assert
            Model.LeftBtnAction(null);
            mockService.Verify(x => x.RotateLeft(), Times.Once);
        }

        [TestMethod]
        public void UserClicksButton_RightBtnClicked_RotateRightShouldBeInvoked()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());

            // act and assert
            Model.RightBtnAction(null);
            mockService.Verify(x => x.RotateRight(), Times.Once);
        }

        [TestMethod]
        public void UserClicksButton_MoveBtnClicked_MoveShouldBeInvoked()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());

            // act and assert
            Model.MoveBtnAction(null);
            mockService.Verify(x => x.Move(), Times.Once);
        }

        [TestMethod]
        public void UserClicksButton_ReportBtnClicked_ConsoleTextShouldBeSet()
            
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            mockService.Setup(ms => ms.GetReport())
                .Returns("Some text about Toy Robot!");
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());

            // act
            Model.ReportBtnAction(null);

            // assert
            Assert.AreEqual("Some text about Toy Robot!\n\n", Model.ConsoleOutputEmulator);
        }

        [TestMethod]
        public void UserClicksButton_ExitBtnClicked_IsWindowClosedShouldBeTrue()
            
        {
            //arrange
            var Model = new ToySimVM();
            var WindowWasClosed = Model.IsWindowClosed;

            // act
            Model.ExitBtnAction(null);

            // assert            
            Assert.AreEqual(false, WindowWasClosed);
            Assert.AreEqual(true, Model.IsWindowClosed);
        }
    }
}
