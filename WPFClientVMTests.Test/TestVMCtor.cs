﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClient.ViewModel;

namespace WPFClientVMTests.Test
{
    [TestClass]
    public class TestVMCtor
    {
        [TestMethod]
        public void VMCtor_Executes_CreatesWPFComboBoxItemsList()
        {
            //arrange
            var Model = new ToySimVM();
            var expectedComboItems = 7;
            Model.GridSize = expectedComboItems;

            // act
            var wpfComboBoxItemsList = Model.GetCoordComboList();

            // assert
            Assert.AreEqual(wpfComboBoxItemsList.Count, expectedComboItems);
            // check vals for combo boxes
            Assert.AreEqual(wpfComboBoxItemsList[0].ComboValue, 0);
            Assert.AreEqual(wpfComboBoxItemsList[wpfComboBoxItemsList.Count - 1].ComboValue, 
                expectedComboItems - 1);
        }

        [TestMethod]
        public void VMCtor_Executes_CreatesBitmapPlaceholder()
        {
            //arrange and act
            var Model = new ToySimVM();
            var bmpImage = Model.CreateTempFlagImage();

            // assert
            Assert.AreEqual(typeof(System.Drawing.Bitmap), bmpImage.GetType());
        }
    }
}
