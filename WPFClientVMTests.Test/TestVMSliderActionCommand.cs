﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClient.ViewModel;
using System.Drawing;
using ToySimWCFService;
using Moq;

namespace WPFClientVMTests.Test
{
    [TestClass]
    public class TestVMSliderActionCommand
    {
        [TestMethod]
        public void SliderAction_WithParamsForBiggerGrid_AddsNewListItems()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            mockService.Setup(ms => ms.SetSquareBoardSize(It.IsAny<int>()))
                .Verifiable();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());
            // set props and build list
            Model.OldGridSize = 6;
            Model.GridSize = 6;
            Model.CoordComboList = Model.GetCoordComboList();

            // act
            Model.GridSize = 8;
            Model.SliderAction(null);

            // assert
            mockService.Verify();
            Assert.AreEqual(8, Model.OldGridSize);
            Assert.AreEqual(8, Model.CoordComboList.Count);
            Assert.AreEqual(7, Model.CoordComboList[Model.CoordComboList.Count - 1].ComboValue);
        }

        [TestMethod]
        public void SliderAction_WithParamsForSmallerGrid_PopsListItems()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            mockService.Setup(ms => ms.SetSquareBoardSize(It.IsAny<int>()))
                .Verifiable();
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());
            Model.OldGridSize = 9;
            Model.GridSize = 9;
            Model.CoordComboList = Model.GetCoordComboList();

            // act
            Model.GridSize = 5;
            Model.SliderAction(null);

            // assert
            mockService.Verify();
            Assert.AreEqual(5, Model.OldGridSize);
            Assert.AreEqual(5, Model.CoordComboList.Count);
            Assert.AreEqual(4, Model.CoordComboList[Model.CoordComboList.Count - 1].ComboValue);
        }
    }
}
