﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFClient.ViewModel;
using Moq;
using ToySimWCFService;
using System.ServiceModel;

namespace WPFClientVMTests.Test
{
    [TestClass]
    public class TestVMConsoleEmulator
    {
        private ToySimVM CreateMock1()
        {
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            mockService.Setup(ms => ms.ProcessCommand(It.IsAny<string[]>()))
                .Returns("Mock 1 output set!");
            mockService.Setup(ms => ms.GetStatus())
                .Returns(() => false);
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());
            return Model;
        }

        [TestMethod]
        public void CommandEntered_IsExitCommand_IsWindowClosedSetToTrue()
        {
            //arrange
            var Model = CreateMock1();
            Model.ConsoleInputEmulator = "Any not null/empty string input";
            var WindowWasClosed = Model.IsWindowClosed;
            
            // act
            Model.ConsoleInputEmulatorAction(null);

            // assert
            Assert.AreEqual(false, WindowWasClosed);
            Assert.AreEqual(true, Model.IsWindowClosed);
        }

        [TestMethod]
        public void CommandEntered_FaultExThrown_ExMessageDisplayed()
        {
            //arrange
            var Model = new ToySimVM();
            var mockService = new Mock<ISimService>();
            mockService.Setup(ms => ms.ProcessCommand(It.IsAny<string[]>()))
                .Throws(new FaultException("Fault ex thrown"));
            Model.ServiceLocator.RegisterService<ISimService>(mockService.Object);
            Model.Initialize(Model.GetService<ISimService>());
            Model.ConsoleInputEmulator = "Any not null/empty string input";
            Model.IsWindowClosed = false;

            //act
            Model.ConsoleInputEmulatorAction(null);

            // assert
            Assert.AreEqual(Model.ConsoleOutputEmulator, "Fault ex thrown\n\n");
        }
    }
}
