﻿using System.Globalization;
namespace Model.Languages.Interface
{
    public interface IToySimLanguage
    {
        System.Collections.ObjectModel
            .ObservableCollection<System.Globalization
            .CultureInfo> GetAvailableCultures();
        void SetCulture(CultureInfo thisCultureInfo);
    }
}
