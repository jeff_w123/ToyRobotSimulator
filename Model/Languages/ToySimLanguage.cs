﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Resources;
using System.Threading;
using Model.Languages.Interface;
using Model.Properties;

namespace Model.Languages
{
    /// <summary>
    /// A class to manage the language options
    /// </summary>
    public class ToySimLanguage : IToySimLanguage
    {
        private CultureInfo cultureInUse;
        private CultureInfo defaultResourceCulture;

        public ToySimLanguage()
        {
            CreateCustomCultures();
            defaultResourceCulture = cultureInUse = Thread.CurrentThread.CurrentCulture;
        }

        // create custom cultures and register them
        private void CreateCustomCultures() 
        {
            //Create a custom culture for sn-ZW (Shona Zimbabwe)
            CultureAndRegionInfoBuilder car1 = new CultureAndRegionInfoBuilder("sh-ZW",
                                                   CultureAndRegionModifiers.None);
            car1.LoadDataFromCultureInfo(CultureInfo.CreateSpecificCulture("en-GB"));
            car1.LoadDataFromRegionInfo(new RegionInfo("GB"));
            car1.CultureEnglishName = "Shona (Zimbabwe)";

            //Create a custom culture for vn-KW (Valyrian Known World)
            CultureAndRegionInfoBuilder car2 = new CultureAndRegionInfoBuilder("vn-KW",
                                                   CultureAndRegionModifiers.None);
            car2.LoadDataFromCultureInfo(CultureInfo.CreateSpecificCulture("en-GB"));
            car2.LoadDataFromRegionInfo(new RegionInfo("GB"));
            car2.CultureEnglishName = "Valyrian (Known World)";

            //Register the cultures
            try
            {
                car1.Register();
                car2.Register();
            }
            catch (InvalidOperationException)
            {
                // NOP: the culture/s are already registered.
            }
        }

        public ObservableCollection<CultureInfo> GetAvailableCultures()
        {
            var result = new ObservableCollection<CultureInfo>();

            ResourceManager rm = new ResourceManager(typeof(Resources));

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            foreach (CultureInfo culture in cultures)
            {
                    // ignore the invariant culture
                    if (culture.Equals(CultureInfo.InvariantCulture))
                        continue;

                    ResourceSet rs = rm.GetResourceSet(culture, true, false);
                    if (rs != null)
                        result.Add(culture);
            }

            if (CultureInfo.DefaultThreadCurrentCulture != null
                && CultureInfo.DefaultThreadCurrentCulture != defaultResourceCulture)
            {
                // insert the default resource, and move the resource in use to the end
                result.Insert(0, defaultResourceCulture);
                result.Move(result.IndexOf(CultureInfo.DefaultThreadCurrentCulture), 
                    result.Count - 1);
            }
            else
            {
                // add culture in use to the end of the list
                result.Add(cultureInUse);
            }
            return result;
        }

        public void SetCulture(CultureInfo thisCulture)
        {
            CultureInfo.DefaultThreadCurrentCulture = thisCulture;
            CultureInfo.DefaultThreadCurrentUICulture = thisCulture;
        }
    }
}
