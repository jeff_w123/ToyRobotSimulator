﻿using System;
using System.ComponentModel;
using Model.Properties;
using Model.ConsoleChecker.Interface;
using Model.Toy;

namespace Model.ConsoleChecker
{
    public class InputParser : IInputParser
    {
        public Command ParseCommand(string[] rawInput)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(Command));
            Command command;

            // best practice for manipulating strings (msdn) says to use uppercase
            var userInput = rawInput[0].ToUpperInvariant();

            // user must not be able to access the enum directly
            if (converter.IsValid(userInput))
                throw new ArgumentException(Resources.CommandErrorMessage);

            try
            {
                object value = converter.ConvertFromString(userInput);
                command = (Command)Enum.Parse(typeof(Command), value.ToString());
            }
            catch
            {
                throw new ArgumentException(Resources.CommandErrorMessage);                     
            }
            return command;
        }

        // Extracts the parameters from the user input and returns it       
        public PlaceCommandParameter ParseCommandParameter( string[] input)
        {
            var thisPlaceCommandParameter = new PlaceCommandParameterParser();     
            return thisPlaceCommandParameter.ParseParameters(input);
        }     
    }
}
