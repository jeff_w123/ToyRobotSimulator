﻿using System;
using System.ComponentModel;
using Model.Properties;
using Model.Toy;

namespace Model.ConsoleChecker
{
    public class PlaceCommandParameterParser
    {        
        private const int parameterCount = 3;
        private const int commandInputCount = 2;

        public PlaceCommandParameter ParseParameters(string[] input)
        {
            Direction direction;
            Position position = null;

            if (input.Length != commandInputCount)
                throw new ArgumentException(Resources.CommandIncompleteMessage);
            var commandParams = input[1].Split(',');
            if (commandParams.Length != parameterCount)
                throw new ArgumentException(Resources.CommandIncompleteMessage);

            // best practice for manipulating strings (msdn) says to use uppercase
            var directionParam = commandParams[commandParams.Length - 1].ToUpperInvariant();

            TypeConverter converter = TypeDescriptor.GetConverter(typeof(Direction));

            // user must not be able to access the enum directly
            if (converter.IsValid(directionParam))
                throw new ArgumentException(Resources.DirectionInvalidMessage); 

            try
            {
                object value = converter.ConvertFromString(directionParam);
                direction = (Direction)Enum.Parse(typeof(Direction), value.ToString());
            }
            catch
            {
                throw new ArgumentException(Resources.DirectionInvalidMessage);
            }
            
            var x = Convert.ToInt32(commandParams[0]);
            var y = Convert.ToInt32(commandParams[1]);
            position = new Position(x, y);

            return new PlaceCommandParameter(position, direction);
        }
    }
}
