﻿using Model.Toy;

namespace Model.ConsoleChecker.Interface
{
    public interface IInputParser
    {
        Command ParseCommand(string[] rawInput);
        PlaceCommandParameter ParseCommandParameter(string[] input);
    }
}
