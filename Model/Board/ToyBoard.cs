﻿using Model.Toy;
using Model.ToyBoard.Interface;

namespace Model.ToyBoard
{
    /// <summary>
    /// This class is the board that the toy sits on.
    /// </summary>
    public class ToyBoard : IToyBoard
    {
        public int Rows { get; private set; }
        public int Columns { get; private set; }

        public ToyBoard(int rows, int columns)
        {
            this.Rows = rows;
            this.Columns = columns;
        }

        public bool IsValidPosition(Position position)
        {
            return position.X < Columns && position.X >= 0 && 
                   position.Y < Rows && position.Y >= 0;
        }
    }
}
