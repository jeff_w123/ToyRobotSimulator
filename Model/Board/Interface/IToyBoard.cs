﻿using Model.Toy;

namespace Model.ToyBoard.Interface
{
    public interface IToyBoard
    {
        bool IsValidPosition(Position position);
    }
}
