﻿using System.ComponentModel;
using Model.Converters;
namespace Model.Toy
{
    // For the converter, these enums must:
    // be in uppercase, and not be any string needed for the language.
    [TypeConverter(typeof(ResourceEnumConverter))]
    public enum Direction
    {
        ENUMNORTH,
        ENUMEAST,
        ENUMSOUTH,
        ENUMWEST
    }
}
