﻿using System;
using Model.Toy.Interface;

namespace Model.Toy
{

    public class ToyRobot : IToyRobot
    {
        public Direction Direction { get; set; }
        public Position Position { get; set; }

        // Sets the toy's position and direction.
        public void Place(Position position, Direction direction)
        {
            this.Position = position;
            this.Direction = direction;
        }

        // returns the next position of the toy based on the direction it's currently facing.
        public Position GetNextPosition()
        {
            var newPosition = new Position(Position.X, Position.Y);
            switch (Direction)
            {
                case Direction.ENUMNORTH:
                    newPosition.Y = newPosition.Y + 1;
                    break;
                case Direction.ENUMEAST:
                    newPosition.X = newPosition.X + 1;
                    break;
                case Direction.ENUMSOUTH:
                    newPosition.Y = newPosition.Y - 1;
                    break;
                case Direction.ENUMWEST:
                    newPosition.X = newPosition.X - 1;
                    break;
            }
            return newPosition;
        }

        // Rotates the direction of the toy 90 degrees to the left.
        public void RotateLeft()
        {
            Rotate(-1);
        }

        // Rotates the direction of the toy 90 degrees to the right.
        public void RotateRight()
        {
            Rotate(1);
        }

        public void Rotate(int rotationNumber)
        {
            var directions = (Direction[])Enum.GetValues(typeof(Direction));
            Direction newDirection;
            if ((Direction + rotationNumber) < 0)
                newDirection = directions[directions.Length - 1];
            else
            {
                var index = ((int)(Direction + rotationNumber)) % directions.Length;
                newDirection = directions[index];
            }
            Direction = newDirection;
        }       
    }
}
