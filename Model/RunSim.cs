﻿using System;
using Model.Factories;

namespace Model
{
    public class RunSim
    {
        public static void Main(string[] args)
        {
            var simulator = ToyFactory.CreateToySim(5,5);

            Console.WriteLine(simulator.GetResourceText("WelcomeMessage"));

            var stopApplication = false;
            while (!stopApplication)
            {
                var command = Console.ReadLine();
                try
                {
                    var output = simulator.ProcessCommand(command.Split(' '));
                    if (simulator.IsRunning == false)
                        stopApplication = true;
                    if (!string.IsNullOrEmpty(output))
                        Console.WriteLine(output);
                }
                catch (ArgumentException exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }
    }
}
