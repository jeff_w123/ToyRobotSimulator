﻿namespace Model.Factories
{
    public static class ToyFactory
    {
        public static ToySim CreateToySim(int x, int y)
        {
            return new ToySim(x,y);
        }
    }
}
