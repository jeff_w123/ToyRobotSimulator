﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Resources;
using Model.Languages;
using Model.Languages.Interface;
using Model.Properties;
using Model.ConsoleChecker;
using Model.ConsoleChecker.Interface;
using Model.Toy;
using Model.Toy.Interface;
using Model.ToyBoard.Interface;

namespace Model.Factories
{
    /// <summary>
    /// Every member implementation complete except for the SquareBoard
    /// </summary>
    public abstract class BaseSim
    {
        public IToyRobot ToyRobot { get; private set; }
        public abstract IToyBoard SquareBoard { get; set; }
        public IInputParser InputParser { get; private set; }
        public Boolean IsRunning { get; set; }
        public IToySimLanguage SimLanguage { get; private set; }

        public BaseSim()
        {
            InputParser = new InputParser();
            ToyRobot = new ToyRobot();
            IsRunning = true;
            SimLanguage = new ToySimLanguage();
        }

        public string ProcessCommand(string[] input)
        {
            var command = InputParser.ParseCommand(input);
            if (command == Command.ENUMEXIT)
                IsRunning = false;
            if (command != Command.ENUMPLACE
                && ToyRobot.Position == null)
                return string.Empty;
            switch (command)
            {
                case Command.ENUMPLACE:
                    var placeCommandParameter = InputParser.ParseCommandParameter(input);
                    if (SquareBoard.IsValidPosition(placeCommandParameter.Position))
                        ToyRobot.Place(placeCommandParameter.Position, placeCommandParameter.Direction);
                    break;
                case Command.ENUMMOVE:
                    var newPosition = ToyRobot.GetNextPosition();
                    if (SquareBoard.IsValidPosition(newPosition))
                        ToyRobot.Position = newPosition;
                    break;
                case Command.ENUMLEFT:
                    ToyRobot.RotateLeft();
                    break;
                case Command.ENUMRIGHT:
                    ToyRobot.RotateRight();
                    break;
                case Command.ENUMREPORT:
                    return GetReport();
            }
            return string.Empty;
        }

        public string GetReport()
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(Direction));
            object value = converter.ConvertToString(ToyRobot.Direction);
            return string.Format(Resources.ReportMessage, ToyRobot.Position.X,
                ToyRobot.Position.Y, value.ToString().ToUpper());
        }

        public System.Drawing.Bitmap GetFlagImage()
        {
            return Resources.PngFlag;
        }

        public string GetResourceText(string resourceName)
        {
            ResourceSet resourceSet = Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            return resourceSet.GetString(resourceName, false);
        }
    }
}
