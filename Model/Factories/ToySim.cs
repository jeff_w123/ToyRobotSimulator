﻿using Model.Factories;
using Model.ToyBoard.Interface;

namespace Model
{
    public class ToySim : BaseSim
    {
        private ToyBoard.ToyBoard thisBoard;

        public ToySim(int x, int y)
        {
            thisBoard = new ToyBoard.ToyBoard(x, y);
        }

        public override IToyBoard SquareBoard
        {
            get 
            { 
                return thisBoard; 
            }
            set 
            { thisBoard = (ToyBoard.ToyBoard)value; 
            }
        }
    }
}
