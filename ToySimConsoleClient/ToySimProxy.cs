﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;
using ToySimWCFService;
using System.Drawing;

namespace ToySimConsoleClient
{
    // WCF create proxy for ISimServiceNoCallback using ClientBase

    public class ToySimProxy : ClientBase<ISimServiceNoCallback>,
        ISimServiceNoCallback
    {
        public string ProcessCommand(string[] input)
        {
            return Channel.ProcessCommand(input);
        }

        public Boolean GetStatus()
        {
            return Channel.GetStatus();
        }

        public void SetIsRunningTrue()
        {
            Channel.SetIsRunningTrue();
        }

        public string GetResourceText(string resourceName)
        {
            return Channel.GetResourceText(resourceName);
        }
    }
}
