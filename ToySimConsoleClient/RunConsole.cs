﻿using System;
using System.ServiceModel;
using ToySimWCFService;

namespace ToySimConsoleClient
{
    class RunConsole
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running console client....\n\n");
            Console.WriteLine("Consuming Toy Simulator Service....\n\n");
            ToySimProxy simulator = ProxyFactory.CreateProxy();
            Console.WriteLine(simulator.GetResourceText("WelcomeMessage"));
            var stopApplication = false;
            try
            {
                while (!stopApplication)
                {
                    var command = Console.ReadLine();
                    try
                    {
                        var output = simulator.ProcessCommand(command.Split(' '));
                        if (simulator.GetStatus() == false)
                            stopApplication = true;
                        if (!string.IsNullOrEmpty(output))
                            Console.WriteLine(output);
                    }
                    catch (FaultException exception)
                    {
                        Console.WriteLine(exception.Message);
                    }
                }
                // reset running flag
                simulator.SetIsRunningTrue();
            }
            catch (CommunicationException commProblem)
            {
                Console.WriteLine("There was a communication problem. " + commProblem.Message + commProblem.StackTrace);
                Console.Read();
            }
        }
    }
}
