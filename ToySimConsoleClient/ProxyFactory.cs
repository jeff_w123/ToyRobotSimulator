﻿namespace ToySimConsoleClient
{
    public static class ProxyFactory
    {
        public static ToySimProxy CreateProxy()
        {
            return new ToySimProxy();
        }
    }
}
