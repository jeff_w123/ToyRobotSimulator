﻿using System;
using System.ServiceModel;
using System.Linq;
namespace ToySimWCFHost
{
    class HostService
    {
        static void Main(string[] args)
        {
            ServiceHost svcHost = null;
            try
            {
                svcHost = new ServiceHost(typeof(ToySimWCFService.SimService));
                svcHost.CloseTimeout = TimeSpan.FromSeconds(1);
                svcHost.Open(); Console.WriteLine("\n\nThe Toy Robot Simulator service is running at the following addresses");
                Console.WriteLine("Available Endpoints :\n");
                svcHost.Description.Endpoints.ToList().ForEach(
                    endpoint => Console.WriteLine(endpoint.Address.ToString()));
            }
            catch (Exception eX)
            {
                svcHost = null;
                Console.WriteLine("Service can not be started.");
                Console.WriteLine("\n\nMake sure that you launch this application in Administrator Mode!");
                Console.WriteLine(" \n\nError Message [" + eX.Message + "]");
                Console.ReadKey();
            }
            if (svcHost != null)
            {
                Console.WriteLine("\nPress any key to close the Service");
                Console.ReadKey();
                try
                {
                    (svcHost as ICommunicationObject).Close();
                }
                catch
                {
                    svcHost.Abort();
                    throw;
                }
                svcHost = null;
            }
        }
    }
}