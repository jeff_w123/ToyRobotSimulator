<h3>Toy Robot Simulator</h3>
<img src = "ToySimWPFClient/toyrobotsim.png" align = right>
This .NET solution is my take on the popular developer test named toy robot simulator. A full requirements specification can be found <a href = "packages/Specification.txt">here.</a>
<br><h4>Solution Synopsis and Download</h4>
My application has a service oriented architecture that uses WCF. This is consumed by a WPF desktop client.
<br><a href = "https://gitlab.com/jeff_w123/ToyRobotSimulator/repository/archive.zip?ref=master">Download Solution Code in Zip</a>
<br><a href = "https://gitlab.com/jeff_w123/ToyRobotSimulator/tree/master">View Solution Files Online</a>
<h4>Acknowledgements</h4>
A full list of sources can be found <a href = "packages/JeffsSources.pdf">here.</a>
<br><h4>Installing and Running</h4>
<br>1. Download and build the application.
<br>2. Launch the the WCF service host found at:
<br>   ToyRobotSimulator/ToySimWCFHost/bin/Debug/ToyRobotService.exe
<br>3. Launch the Toy Robot WPF window found at:
<br>   ToyRobotSimulator/ToySimWPFClient/bin/Debug/ToyRobotClient.exe
<br>
<br>Note: The service requires admin rights.
<br><h4>User Instructions</h4>
The toy robot can be controlled by both the Console Constrols and GUI Controls. The console user commands are covered in the <a href = "packages/Specification.txt">specification.</a>
<br><h4>GUI Robot Controls (For English)</h4>
PLACE - To enter the placement coordinates click any position on the grid. Alternatively, you can individually select the x and y coordinate combo boxes. A direction must also be clicked to enable the PLACE button.
<br>MOVE, REPORT, LEFT, RIGHT: To enable these buttons the robot must be placed.
<br>EXIT - allows the user to exit at any time.
<br>Change grid size - Use the slider. Note: If the robot is outside of the selected size then it will be placed in a valid position.
<br>Change Language - Select from the language combo box.
<br>Note: Language changes affect the console commands too!
<h4>Supported Operating Systems</h4>
Toy Robot Simulator should run on any Windows operating system. It has been tested on Windows 10 Home Edition 32-bit.
<h4>Licence</h4>
Toy Robot Simulator is licensed under the GNU Lesser General Public License v.3.0<br>
The GPL is specifically designed to reduce the usefulness of GPL-licensed code to closed-source, proprietary software. The BSD license (and similar) do not mandate code-sharing if the BSD-licensed code is modified by licensees. The LGPL achieves the best of both worlds: an LGPL-licensed library can be incorporated within closed-source proprietary code, and yet those using an LGPL-licensed library are required to release source code to that library if they change it.
