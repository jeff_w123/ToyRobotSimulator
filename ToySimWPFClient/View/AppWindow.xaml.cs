﻿using System.Windows;
using ToySimWCFService;
using WPFClient.Model;
using WPFClient.ViewModel;
namespace WPFClient
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AppWindow : Window
    {
        protected ToySimVM Model
        {
            get { return (ToySimVM)Resources["viewModel"]; }
        }
 
        public AppWindow()
        {        
            InitializeComponent();

            // create/register a service then inject it into the VM using the service locator
            var dataService = ProxyFactory.CreateProxy(Model, "WsHttpDualBindingCallbackServices");
            Model.ServiceLocator.RegisterService<ISimService>(dataService);
            Model.Initialize(Model.GetService<ISimService>());

            Loaded += delegate { Model.OnLoaded(); };
            Unloaded += delegate { Model.OnUnloaded(); };
        }
    }
}
