﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace WPFClient.View.Behaviours
{
    public class ToyGrid:Grid
    {
        #region Dependency Property: MaxGridSize
        public int MaxGridSize
        {
            // do NOT place logic here
            get 
            { 
                return (int)GetValue(MaxGridSizeProperty); 
            }
            set 
            { 
                SetValue(MaxGridSizeProperty, value); 
            }
        }

        public static readonly DependencyProperty MaxGridSizeProperty =
            DependencyProperty.Register("MaxGridSize", typeof(int), typeof(ToyGrid), new PropertyMetadata(0));

        DependencyPropertyDescriptor maxGridSize = DependencyPropertyDescriptor.
        FromProperty(ToyGrid.MaxGridSizeProperty, typeof(ToyGrid)); 
        #endregion

        #region Dependency Property SelectedSize
        public int SelectedSize
        {
            // do NOT place logic here
            get 
            { 
                return (int)GetValue(SelectedSizeProperty); 
            }
            set 
            { 
                SetValue(SelectedSizeProperty, value); 
            }
        }

        public static readonly DependencyProperty SelectedSizeProperty =
            DependencyProperty.Register("SelectedSize", typeof(int), typeof(ToyGrid), new PropertyMetadata(0));

        DependencyPropertyDescriptor selectedSize = DependencyPropertyDescriptor.
        FromProperty(ToyGrid.SelectedSizeProperty, typeof(ToyGrid)); 
        #endregion

        // a property to use in grid resize logic
        private int visibleOldGridSize;

        #region Ctor
        public ToyGrid()
        {
            Height = 300;
            Width = 300;

            // bindings values only evaluated after initialisation of object
            if (selectedSize != null)
            {
                selectedSize.AddValueChanged(this, delegate
                {
                    if (MaxGridSize != 0)
                    {
                        SetVisibleSize(SelectedSize);
                    }
                });
            }

            if (maxGridSize != null)
            {
                maxGridSize.AddValueChanged(this, delegate
                {
                    if (MaxGridSize != 0)
                    {
                        GenerateMaxSizeGrid();
                    }
                });
            }
        } 
        #endregion
        
        private void GenerateMaxSizeGrid()
        {
            CreateToyGrid(MaxGridSize);
            visibleOldGridSize = MaxGridSize;
            AddGridButtons();
            SetVisibleSize(SelectedSize);
        }

        private void SetVisibleSize(int selectedSize)
        {
            var sizeDifference = visibleOldGridSize - selectedSize;
            switch (Math.Sign(sizeDifference))
            {
                case 0:
                    // grid is same size - do nothing
                    return;
                case 1:
                    // grid is getting smaller
                    var buttonsToCollapse = new List<UIElement>();
                    for (int y = selectedSize; y < visibleOldGridSize; y++)
                    {
                        var x = ConvertColumnToRow(y);
                        var theseButtons = Children.Cast<UIElement>()
                            .Where(e => Grid.GetRow(e) == x || Grid.GetColumn(e) == y).ToList();
                        theseButtons.ForEach(b => buttonsToCollapse.Add(b));
                        // set row height
                        RowDefinitions[x].Height = new GridLength(0, GridUnitType.Auto);
                        // set column width
                        ColumnDefinitions[y].Width = new GridLength(0, GridUnitType.Auto);
                    }
                    // set buttons to collapse
                    buttonsToCollapse.ForEach(b => b.Visibility = Visibility.Collapsed);
                    visibleOldGridSize = selectedSize;
                    return;
                case -1:
                    // grid is getting bigger
                    var buttonsToVisible = new List<UIElement>();
                    for (int y = visibleOldGridSize; y < selectedSize; y++)
                    {
                        var x = ConvertColumnToRow(y);
                        var theseButtons = Children.Cast<UIElement>()
                            .Where(e => Grid.GetRow(e) == x
                            && Grid.GetColumn(e) <= y
                            || Grid.GetColumn(e) == y
                            && Grid.GetRow(e) >= x).ToList();
                        theseButtons.ForEach(b => buttonsToVisible.Add(b));
                        // set row height
                        RowDefinitions[x].Height = new GridLength(1, GridUnitType.Star);
                        // set column width
                        ColumnDefinitions[y].Width = new GridLength(1, GridUnitType.Star);
                    }
                    // set buttons to visible
                    buttonsToVisible.ForEach(b => b.Visibility = Visibility.Visible);
                    visibleOldGridSize = selectedSize;
                    return;
                default:
                    break;
            }
        }

        // WPF grid and ToyRobotGrid are different: TRG row numbers are inverted
        private int ConvertColumnToRow(int columnInt)
        {
            return MaxGridSize - 1 - columnInt;
        }
        
        private void CreateToyGrid(int size)
        {
            for (int i = 0; i < size; i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition());
                RowDefinitions.Add(new RowDefinition());
            }
        }

        private void AddGridButtons()
        {
            // add a button to each grid cell
            for (int i = 0; i < MaxGridSize; i++)
            {
                for (int j = 0; j < MaxGridSize; j++)
                {
                    var thisButton = new GridButton(j, ConvertColumnToRow(i));
                    SetRow(thisButton, i);
                    SetColumn(thisButton, j);
                    Children.Add(thisButton);
                }
            }
        }
    }
}
