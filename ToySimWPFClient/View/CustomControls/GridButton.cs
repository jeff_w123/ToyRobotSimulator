﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using ToySimWCFService;
using System.Reflection;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace WPFClient.View.Behaviours
{
    public class GridButton:Button
    {
        public int ToyGridPositionX { get; private set; }
        public int ToyGridPositionY { get; private set; }
        public Bitmap RobotPng { get; private set; }

        // This is a toy robot dependency property that binds to the VM
        #region GridToyRobot Dependency Property
        public SVCToyRobot GridToyRobot
        {
            get 
            { 
                return (SVCToyRobot)GetValue(GridToyRobotProperty); 
            }
            set 
            { 
                SetValue(GridToyRobotProperty, value); 
            }
        }

        public static readonly DependencyProperty GridToyRobotProperty =
            DependencyProperty.Register("GridToyRobot", typeof(SVCToyRobot),
            typeof(GridButton), new PropertyMetadata(null)); 
        #endregion

        DependencyPropertyDescriptor gridToyRobot = DependencyPropertyDescriptor.
            FromProperty(GridButton.GridToyRobotProperty, typeof(GridButton));

        #region GridButton Ctor
        public GridButton(int x, int y) 
        {
            // set properties
            ToyGridPositionX = x;
            ToyGridPositionY = y;

            DisplayToyRobotGridPosition();

            var defaultNorthRobotBmp = GetStoredBitmap();

            gridToyRobot.AddValueChanged(this, delegate
            {
                if (GridToyRobot.RobotPosition.Y == ToyGridPositionY
                    && GridToyRobot.RobotPosition.X == ToyGridPositionX)
                {
                    // reset the robot direction before rotating
                    RobotPng = defaultNorthRobotBmp;

                    // do the appropriate rotation
                    switch ((int)GridToyRobot.RobotDirection)
                    {
                        case (int)WPFClient.Utilities.WPFDirection.North:
                            // already facing north, do nothing
                            RobotPng = RotateImage(RobotPng, RotateFlipType.RotateNoneFlipNone);
                            break;
                        case (int)WPFClient.Utilities.WPFDirection.East:
                            // turn 90
                            RobotPng = RotateImage(RobotPng, RotateFlipType.Rotate90FlipNone);
                            break;
                        case (int)WPFClient.Utilities.WPFDirection.South:
                            // turn 180 - just flip the symmetric image
                            RobotPng = RotateImage(RobotPng, RotateFlipType.RotateNoneFlipY);
                            break;
                        case (int)WPFClient.Utilities.WPFDirection.West:
                            // turn 270
                            RobotPng = RotateImage(RobotPng, RotateFlipType.Rotate270FlipNone);
                            break;
                        default:
                            break;
                    }

                    // display the rotated robot image
                    var robotSource = System.Windows.Interop.
                        Imaging.CreateBitmapSourceFromHBitmap
                        (RobotPng.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.
                        FromEmptyOptions());
                    var controlImage = new System.Windows.Controls.Image();
                    controlImage.Source = robotSource;
                    Content = controlImage;
                }
                else
                {
                    // show the grid position instead
                    DisplayToyRobotGridPosition();
                };
            });
        }

        public Bitmap GetStoredBitmap()
        {
            // io components for embedded image
            Assembly assembly;
            assembly = Assembly.GetExecutingAssembly();
            var imageStream = assembly.GetManifestResourceStream("WPFClient.View.Images.robot.png");
            var robotPng = System.Drawing.Image.FromStream(imageStream, true, true);
            return new Bitmap(robotPng);
        }

        private void DisplayToyRobotGridPosition()
        {
            // show the grid position in the content
            Content = String.Format("{0} , {1}", ToyGridPositionX.ToString(),
            ToyGridPositionY.ToString());
        }
        #endregion

        private Bitmap RotateImage(System.Drawing.Image img, RotateFlipType thisRotation)
        {
            var bmp = new Bitmap(img);
            using (Graphics gfx = Graphics.FromImage(bmp))
            {
                gfx.Clear(System.Drawing.Color.Transparent);
                gfx.DrawImage(img, 0, 0, img.Width, img.Height);
            }
            bmp.RotateFlip(thisRotation);
            return bmp;
        }
    }
}
