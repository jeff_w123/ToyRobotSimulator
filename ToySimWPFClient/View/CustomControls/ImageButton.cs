﻿using System.Windows.Controls;
using System.Windows.Media;

namespace WPFClient.View.Behaviours
{
    /// <summary>
    /// This image button has a transparent background and no border
    /// </summary>
    public class ImageButton:Button
    {
        Image thisImage = null;

        public ImageButton()
        {
            // add an image
            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Horizontal;
            panel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            thisImage = new Image();
            panel.Children.Add(thisImage);
            Content = panel;

            // remove background and border
            Background = null;
            BorderThickness = new System.Windows.Thickness(0.0);
        }

        public ImageSource Image
        {
            get
            {
                if (thisImage != null)
                    return thisImage.Source;
                else
                    return null;
            }
            set
            {
                if (thisImage != null)
                    thisImage.Source = value;
            }
        }
    }
}
