﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace WPFClient.View.Behaviours
{
    /// <summary>
    /// A class to invoke a command on the VM when a combo box value changes
    /// </summary>
    public class ComboValueChangedBehaviour : Behavior<ComboBox>
    {
        /// <summary>
        /// DataBindable Command
        /// </summary>

        public ICommand ComboCommand
        {
            get 
            { 
                return (ICommand)GetValue(CommandProperty); 
            }
            set 
            { 
                SetValue(CommandProperty, value); 
            }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "ComboCommand",
            typeof(ICommand),
            typeof(ComboValueChangedBehaviour),
            new PropertyMetadata(null));

        /// <summary>
        /// On behavior attached
        /// </summary>
        protected override void OnAttached()
        {
            this.AssociatedObject.SelectionChanged += this.OnValueChanged;
            base.OnAttached();
        }

        /// <summary>
        /// On behavior detaching.
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.SelectionChanged -= this.OnValueChanged;
        }

        /// <summary>
        /// Occurs when the combo value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValueChanged(object sender, SelectionChangedEventArgs e)
        {
                ExecuteCommand();
        }

        private void ExecuteCommand()
        {
            if (this.ComboCommand != null)
                this.ComboCommand.Execute(null);
        }
    }
}