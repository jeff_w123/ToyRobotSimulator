﻿using System.Windows;
using System.Windows.Controls;

namespace WPFClient.View.Behaviours
{
    /// <summary>
    /// Class to define the scrolling behaviour of the console output text box.
    /// Uses a bindable boolean DP to set scroll to end, or scroll to home.
    /// </summary>
    public class ScrollToEndBehavior
    {
        public static readonly DependencyProperty OnTextChangedProperty =
             DependencyProperty.RegisterAttached(
             "OnTextChanged",
             typeof(bool),
             typeof(ScrollToEndBehavior),
             new UIPropertyMetadata(false, OnTextChanged)
             );

        public static bool GetOnTextChanged(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(OnTextChangedProperty);
        }

        public static void SetOnTextChanged(DependencyObject dependencyObject,
            bool value)
        {
            dependencyObject.SetValue(OnTextChangedProperty, value);
        }

        private static void OnTextChanged(DependencyObject dependencyObject, 
            DependencyPropertyChangedEventArgs e)
        {
            var textBox = dependencyObject as TextBox;
            var newValue = (bool)e.NewValue;

            if (textBox == null || (bool)e.OldValue == newValue)
                return;

            // use 2 handlers to set the scrolling behaviour
            TextChangedEventHandler scrollDownHandler = (object sender, TextChangedEventArgs args) =>
                ((TextBox)sender).ScrollToEnd();

            TextChangedEventHandler scrollUpHandler = (object sender, TextChangedEventArgs args) =>
                ((TextBox)sender).ScrollToHome();

            if (newValue)
            {
                textBox.TextChanged += scrollDownHandler;
                textBox.TextChanged -= scrollUpHandler;
            }
            else
            {
                textBox.TextChanged -= scrollDownHandler;
                textBox.TextChanged += scrollUpHandler;
            }
        }

    }
}
