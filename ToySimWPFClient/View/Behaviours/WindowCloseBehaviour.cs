﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace WPFClient.View.Behaviours
{
    /// <summary>
    /// Class to define the window close behavior.
    /// DP can be bound to a prop in the VM to enable unit testing.
    /// </summary> 
    public class WindowCloseBehaviour : Behavior<StackPanel>
    {
        public bool IsWindowClosed
        {
            get 
            { 
                return (bool)GetValue(IsWindowClosedProperty); 
            }
            set 
            { 
                SetValue(IsWindowClosedProperty, value); 
            }
        }

        public static readonly DependencyProperty IsWindowClosedProperty =
            DependencyProperty.Register("IsWindowClosed", typeof(bool), 
            typeof(WindowCloseBehaviour), 
            new PropertyMetadata(false, OnIsWindowClosedChanged));

        private static void OnIsWindowClosedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = d as WindowCloseBehaviour;

            if (behavior != null)
                behavior.OnIsWindowClosedChanged();
        }

        private void OnIsWindowClosedChanged()
        {
            // when IsWindowClosed is true, find the window in the visual tree, then close it.
            if (IsWindowClosed)
            {
                Window parentWindow = Window.GetWindow(AssociatedObject);
                parentWindow.Close();
            }
        }
    }
}
