﻿    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interactivity;

namespace WPFClient.View.Behaviours
{
    /// <summary>
    /// Custom behaviour class exposes command and value properties
    /// used to raise event when slider value is changed.
    /// courtesy of anonymous developer @ https://gist.github.com/4326429
    /// </summary>
    public class SliderValueChangedBehavior : Behavior<Slider>
    {
        #region Dependency property Value

        /// <summary>
        /// DataBindable value
        /// </summary>
        public double Value
        {
            get 
            { 
                return (double)GetValue(ValueProperty); 
            }
            set 
            { 
                SetValue(ValueProperty, value); 
            }
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(double),
            typeof(SliderValueChangedBehavior));

        #endregion

        #region Dependency property Value

        /// <summary>
        /// DataBindable Command
        /// </summary>
        public ICommand Command
        {
            get 
            { 
                return (ICommand)GetValue(CommandProperty); 
            }
            set 
            { 
                SetValue(CommandProperty, value); 
            }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command",
            typeof(ICommand),
            typeof(SliderValueChangedBehavior),
            new PropertyMetadata(null));

        #endregion

        /// <summary>
        /// On behavior attached
        /// </summary>
        protected override void OnAttached()
        {
            this.AssociatedObject.ValueChanged += this.OnValueChanged;
            base.OnAttached();
        }

        /// <summary>
        /// On behavior detaching.
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.ValueChanged -= this.OnValueChanged;
        }

        /// <summary>
        /// Occurs when the slider's value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Mouse.Captured != null)
            {
                this.AssociatedObject.LostMouseCapture += this.OnLostMouseCapture;
            }
            else
            {
                ExecuteCommand();
            }
        }

        private void OnLostMouseCapture(object sender, MouseEventArgs e)
        {
            this.AssociatedObject.LostMouseCapture -= this.OnLostMouseCapture;
            ExecuteCommand();
        }

        private void ExecuteCommand()
        {
            this.Value = this.AssociatedObject.Value;
            if (this.Command != null)
                this.Command.Execute(this.Value);
        }
    }
}

