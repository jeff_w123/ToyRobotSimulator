﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.ServiceModel;
using System.Windows.Input;
using ToySimWCFService;
using WPFClient.Utilities;

namespace WPFClient.ViewModel
{
    /// <summary>
    /// This view model uses a proxy and duplex WCF service to access the model.
    /// </summary>

    // This VM used for WCF call back context
    [CallbackBehaviorAttribute(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class ToySimVM : VMbase
    {
        private bool isLoaded = false;
        public ISimService SimulatorProxy { get; set; }

        #region Ctor: Relay commands and property setting with client constants
        public ToySimVM()
        {
            /// <summary>
            /// Constructor for VM
            /// </summary>
            // initialise relay commands
            DirectionActionCommand = new RelayCommand(DirectionAction, CanActionTrue);
            GridBtnActionCommand = new RelayCommand(GridBtnAction, CanActionTrue);
            SliderActionCommand = new RelayCommand(SliderAction, CanActionTrue);
            ConsoleInputActionCommand = new RelayCommand(ConsoleInputEmulatorAction, CanActionTrue);
            ReportBtnActionCommand = new RelayCommand(ReportBtnAction, CanActionIsRobotPlaced);
            MoveBtnActionCommand = new RelayCommand(MoveBtnAction, CanActionIsRobotPlaced);
            RightBtnActionCommand = new RelayCommand(RightBtnAction, CanActionIsRobotPlaced);
            LeftBtnActionCommand = new RelayCommand(LeftBtnAction, CanActionIsRobotPlaced);
            PlaceBtnActionCommand = new RelayCommand(PlaceBtnAction, CanActionArePlaceParamsSet);
            ExitBtnActionCommand = new RelayCommand(ExitBtnAction, CanActionTrue);
            LangComboActionCommand = new RelayCommand(LangComboAction, CanActionTrue);
            
            // get and use client constants
            OldGridSize = GridSize = ClientConstants.sliderDefaultValue;

            // use the grid size to set the coordinate combo box vals
            CoordComboList = GetCoordComboList();

            // give the flag a temp image to be later updated by INPC
            Flag = CreateTempFlagImage();
        }
        #endregion constructor

        #region Initialize(ISimService someService) : VM lifecycle Method
        public void Initialize(ISimService dataService)
        {
            // use the injected service
            SimulatorProxy = dataService;
        } 
        #endregion

        #region OnLoaded() : VM lifecycle Method
        public void OnLoaded()
        {
            if (!isLoaded)
            {
                // loaded code here 
                var availableCultures = SimulatorProxy.GetAvailableCultures();
                LanguageComboList = GetLanguageComboList(availableCultures);
                LanguageComboValue = LanguageComboList.Count - 1;
                LangSelected = LanguageComboList[LanguageComboValue].ComboCultureValue;
                GetLabelProperties().ForEach(p => p.SetValue(this, SimulatorProxy.GetResourceText(p.Name)));
                ToyRobotSim = SimulatorProxy.GetToyRobot();
                // check if robot is already placed
                if (ToyRobotSim != null)
                {
                    // get current model position & make sure it sits on board, then set it.
                    var thisPosition = ToyRobotSim.RobotPosition;
                    if (thisPosition.X > GridSize - 1)
                    {
                        thisPosition.X = GridSize - 1;
                    }
                    if (thisPosition.Y > GridSize - 1)
                    {
                        thisPosition.Y = GridSize - 1;
                    }
                    ToyRobotSim.RobotPosition = thisPosition;

                    // callback on setToyRobot will set local ToyRobot property
                    SimulatorProxy.SetToyRobot(ToyRobotSim);
                }

                // set toy board size to WPF config user value
                SimulatorProxy.SetSquareBoardSize(GridSize);

                isLoaded = true;
            }
        } 
        #endregion

        #region OnUnloaded() : VM lifecycle Method
        public void OnUnloaded()
        {
            if (isLoaded)
            {
                // cleanup/unloaded code here 
                isLoaded = false;
            }
        } 
        #endregion

        #region RefreshRobot() : Callback Method, mandatory for base:IContractCallback 
        public override void RefreshRobot()
        {
            /// <summary>
            /// Callback method to update the VM Toy Tobot property.
            /// </summary>
            if (SimulatorProxy.GetToyRobot()!=null)
            {
                ToyRobotSim = SimulatorProxy.GetToyRobot();
            }
        }
        #endregion

        #region Create Temp Flag Method
        public Bitmap CreateTempFlagImage()
        {
            // create a placeholder bmp to set flag image
            // INPC will update this onloaded
            var bmp = new Bitmap(114, 65);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 114, 65);
                graph.FillRectangle(Brushes.White, ImageSize);
            }
            return bmp;
        } 
        #endregion

        #region Robot: Service robot property
        /// <summary>
        /// Robot Properties: bindings for service robot
        /// </summary>
        private SVCToyRobot toyRobotSim;
        public SVCToyRobot ToyRobotSim
        {
            get
            {
                return toyRobotSim;
            }

            set
            {
                if (value != toyRobotSim)
                {
                    toyRobotSim = value;
                }
                RaisePropertyChanged(() => ToyRobotSim);
            }
        }
        #endregion Robot: Service robot property

        #region Flag: image property
        /// <summary>
        /// Flag Image Property: bindings for flag image
        /// </summary>
        private Image flag;
        public Image Flag
        {
            get
            {
                return flag;
            }

            set
            {
                if (value != flag)
                {
                    flag = value;
                }
                RaisePropertyChanged(() => Flag);
            }
        }
        #endregion Flag: image property

        #region Console Output Emulator Textbox: text properties

        private string consoleOutputEmulator;
        public string ConsoleOutputEmulator
        {
            get { return consoleOutputEmulator; }

            set
            {
                if (consoleOutputEmulator != value)
                {
                    // logic to clear the box, or append the string
                    if (value == string.Empty)
                    {
                        consoleOutputEmulator = value;
                    }
                    else
                    {
                        consoleOutputEmulator += value + "\n\n"; 
                    }
                    RaisePropertyChanged(() => ConsoleOutputEmulator); 
                }
            }
        }
        #endregion Console Output Emulator Textbox

        #region Console Input Emulator Textbox: text properties

        private string consoleInputEmulator;
        public string ConsoleInputEmulator
        {
            get { return consoleInputEmulator; }

            set
            {
                if (value != consoleInputEmulator)
                {
                    consoleInputEmulator = value;
                    RaisePropertyChanged(() => ConsoleInputEmulator);
                }
            }
        }
        #endregion Console Input Emulator Textbox

        #region Direction buttons: isEnabled properties
        /// <summary>
        /// Direction Button Properties: bindings for isEnabled
        /// </summary>

        private bool directionNorthIsEnabled = true;
        public bool DirectionNorthManifest
        {
            get { return directionNorthIsEnabled; }

            set
            {
                if (value != directionNorthIsEnabled)
                {
                    directionNorthIsEnabled = value;
                    RaisePropertyChanged(() => DirectionNorthManifest);
                }
            }
        }

        private bool directionEastIsEnabled = true;
        public bool DirectionEastManifest
        {
            get { return directionEastIsEnabled; }

            set
            {
                if (value != directionEastIsEnabled)
                {
                    directionEastIsEnabled = value;
                    RaisePropertyChanged(() => DirectionEastManifest);
                }
            }
        }

        private bool directionSouthIsEnabled = true;
        public bool DirectionSouthManifest
        {
            get { return directionSouthIsEnabled; }

            set
            {
                if (value != directionSouthIsEnabled)
                {
                    directionSouthIsEnabled = value;
                    RaisePropertyChanged(() => DirectionSouthManifest);
                }
            }
        }

        private bool directionWestIsEnabled = true;
        public bool DirectionWestManifest
        {
            get { return directionWestIsEnabled; }

            set
            {
                if (value != directionWestIsEnabled)
                {
                    directionWestIsEnabled = value;
                    RaisePropertyChanged(() => DirectionWestManifest);
                }
            }
        }
        #endregion

        #region Direction buttons: selectedValue properties
        /// <summary>
        /// Direction Button Properties: bindings for selectedValue
        /// </summary>
        // set this to null to represent not yet selected.
        private int? directionSelectedValue = null;
        public int? DirectionSelectedValue
        {
            get { return directionSelectedValue; }

            set
            {
                if (value != directionSelectedValue)
                {
                    directionSelectedValue = value;
                    RaisePropertyChanged(() => DirectionSelectedValue);
                }
            }
        }
        #endregion

        #region Slider: Max and min properties
        /// <summary>
        /// Slider Properties: read only bindings for max and min
        /// </summary>
        public int SliderMinValue
        {
            get { return ClientConstants.minGridSize; }
        }
        public int SliderMaxValue
        {
            get { return ClientConstants.maxGridSize; }
        }
        #endregion

        #region Direction buttons: Command Parameters properties
        /// <summary>
        /// Direction Button Properties: read only bindings for Command Parameters
        /// </summary>
        public WPFDirection NorthDirection
        {
            get { return WPFDirection.North; }
        }

        public WPFDirection EastDirection
        {
            get { return WPFDirection.East; }
        }

        public WPFDirection SouthDirection
        {
            get { return WPFDirection.South; }
        }

        public WPFDirection WestDirection
        {
            get { return WPFDirection.West; }
        }
        #endregion

        #region ICommand properties
        /// <summary>
        /// RelayCommands that are used by various controls
        /// </summary>
        public ICommand DirectionActionCommand { get; private set; }
        public ICommand GridBtnActionCommand { get; private set; }
        public ICommand SliderActionCommand { get; private set; }
        public ICommand ConsoleInputActionCommand { get; private set; }
        public ICommand ReportBtnActionCommand { get; private set; }
        public ICommand MoveBtnActionCommand { get; private set; }
        public ICommand LeftBtnActionCommand { get; private set; }
        public ICommand RightBtnActionCommand { get; private set; }
        public ICommand PlaceBtnActionCommand { get; private set; }
        public ICommand ExitBtnActionCommand { get; private set; }
        public ICommand LangComboActionCommand { get; private set; }
        #endregion

        #region Can Action: properties - always returns true
        private bool CanActionTrue(object parameter)
        {
            return true;
        }
        #endregion

        #region Can Action Is Robot Placed?: returns bool
        public bool CanActionIsRobotPlaced(object parameter)
        {
            if (ToyRobotSim != null)
            return true;
            return false;
        }
        #endregion

        #region Can Action Are Place Params Set?: returns bool
        public bool CanActionArePlaceParamsSet(object parameter)
        {
            // just test for any nulls
            if (ComboBoxXValue != null
                && ComboBoxYValue != null
                && DirectionSelectedValue != null)
            return true;
            return false;
        }
        #endregion

        #region Direction buttons': Command Action method
        public void DirectionAction(object parameter)
        {
            SetDirectionsRadiosIsEnabled();
            var thisDirection = Convert.ToInt32(parameter);
            // set the DirectionSelectedValue to false
            DirectionSelectedValue = thisDirection;
            switch (thisDirection)
            {
                case 0:
                    DirectionNorthManifest = false;
                    break;
                case 1:
                    DirectionEastManifest = false;
                    break;
                case 2:
                    DirectionSouthManifest = false;
                    break;
                case 3:
                    DirectionWestManifest = false;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Directions Buttons: Method to set isEnabled
        private void SetDirectionsRadiosIsEnabled()
        {
            DirectionNorthManifest = true;
            DirectionEastManifest = true;
            DirectionSouthManifest = true;
            DirectionWestManifest = true;
        }
        #endregion

        #region Grid Button: Command Action method
        /// <summary>
        /// Grid Button - Command Action
        /// </summary>
        /// <param name="parameter"></param>
        private void GridBtnAction(object parameter)
        {
            var gridPosition = parameter as WPFPosition;
            ComboBoxXValue = gridPosition.GridX;
            ComboBoxYValue = gridPosition.GridY;
        }
        #endregion

        #region Slider: Command Action method
        /// <summary>
        /// Slider - Command Action
        /// </summary>
        /// <param name="parameter"></param>
        public void SliderAction(object parameter)
        {
            // check and reset combobox selected values
            SetComboBoxInLimits(GridSize - 1);

            // set toy board size
            SimulatorProxy.SetSquareBoardSize(GridSize);

            // repopulate combobox lists based on new grid size 
            var sizeDifference = GridSize - OldGridSize;
            switch (Math.Sign(sizeDifference))
            {
                case 0:
                    // do nothing
                    return;
                case -1:
                    //small grid
                    var noOfObjectsToRemove = Math.Abs(sizeDifference);
                    // remove the last items in the list
                    for (int i = 0; i < noOfObjectsToRemove; i++)
                    {
                        CoordComboList.RemoveAt(CoordComboList.Count - 1);
                    }
                    // check current toy position and set to valid if needed
                    SetPositionToValid(GridSize);
                    OldGridSize = GridSize;
                    return;
                case 1:
                    // big grid
                    // add new items to the combo list
                    for (int i = OldGridSize; i < GridSize; i++)
                    {
                        var thisItem = new WPFComboBoxItem();
                        thisItem.ComboValue = i;
                        CoordComboList.Add(thisItem);
                    }
                    OldGridSize = GridSize;
                    return;
                default:
                    break;
            }
        }
        #endregion

        #region Language Combo: Command Action method
        /// <summary>
        /// Language Combo - Command Action
        /// </summary>
        /// <param name="parameter"></param>
        private void LangComboAction(object parameter)
        {
            // get and set culture from language combo box object
            LangSelected = LanguageComboList[LanguageComboValue].ComboCultureValue;
            SimulatorProxy.SetCulture(LangSelected);

            // update the textblock labels
            GetLabelProperties().ForEach(p => p.SetValue(this, SimulatorProxy.GetResourceText(p.Name)));

            // clear display
            ConsoleOutputEmulator = string.Empty;

            // display new language welcome message and show flag
            DoScrollToBottom = false;
            ConsoleOutputEmulator = SimulatorProxy.GetResourceText("WelcomeMessage");
            Flag = SimulatorProxy.GetFlagImage();
        }
        #endregion

        #region UI Labels: Get properties method
        /// <summary>
        /// Method to get only properties that are
        /// designated for localized labels
        /// </summary>
        /// <returns></returns>
        private List<PropertyInfo> GetLabelProperties()
        {
            var propertyList = new List<PropertyInfo>();
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.PropertyType == typeof(string)
                    && prop.Name.Substring(prop.Name.Length - 5, 5) == "Label")
                {
                    propertyList.Add(prop);
                }
            }
            return propertyList;
        }
        #endregion

        #region Console Input Textblock: Command Action method
        /// <summary>
        /// Console Input Emulator - Command Action
        /// </summary>
        public void ConsoleInputEmulatorAction(object parameter)
        {
            DoScrollToBottom = true;
            try
            {
                if (!string.IsNullOrEmpty(ConsoleInputEmulator))
                {
                    var output = SimulatorProxy.ProcessCommand(ConsoleInputEmulator.Split(' '));
                    // check if service robot object is running
                    if (SimulatorProxy.GetStatus() == false)
                    {
                        // reset service flag before exiting
                        SimulatorProxy.SetIsRunningTrue();
                        // exit the app
                        IsWindowClosed = true;
                    }                    
                    if (!string.IsNullOrEmpty(output))
                    {
                        ConsoleOutputEmulator = output;
                    }
                    // get robot state
                    RefreshRobot();
                }
            }
            catch (FaultException exception)
            {
                ConsoleOutputEmulator = exception.Message;
            }
            ConsoleInputEmulator = string.Empty;
        }
        #endregion

        #region Report Button: Command Action method
        /// <summary>
        /// Report Button - Command Action
        /// </summary>
        public void ReportBtnAction(object parameter)
        {
            DoScrollToBottom = true;
            ConsoleOutputEmulator = SimulatorProxy.GetReport();
        }
        #endregion

        #region Move Button: Command Action method
        /// <summary>
        /// Move Button - Command Action
        /// </summary>
        public void MoveBtnAction(object parameter)
        {
            SimulatorProxy.Move();
        }
        #endregion

        #region Rotate Left Button: Command Action method
        /// <summary>
        /// Left Button - Command Action
        /// </summary>
        public void LeftBtnAction(object parameter)
        {
            SimulatorProxy.RotateLeft();
        }
        #endregion

        #region Rotate Right Button: Command Action method
        /// <summary>
        /// Right Button - Command Action
        /// </summary>
        public void RightBtnAction(object parameter)
        {
            SimulatorProxy.RotateRight();
        }
        #endregion

        #region Place Button: Command Action method
        /// <summary>
        /// Place Button - Command Action
        /// </summary>
        public void PlaceBtnAction(object parameter)
        {
            var thisRobot = new SVCToyRobot();
            thisRobot.RobotPosition = new SVCPosition((int)ComboBoxXValue,
                (int)ComboBoxYValue);
            thisRobot.RobotDirection = (int)DirectionSelectedValue;
            ToyRobotSim = thisRobot;
            SimulatorProxy.SetToyRobot(ToyRobotSim);
        }
        #endregion

        #region Exit Button: Command Action method
        /// <summary>
        /// Exit Button - Command Action
        /// </summary>
        public void ExitBtnAction(object parameter)
        {
            IsWindowClosed = true;
        }
        #endregion

        #region Combo Box Values: Method to reset values on grid resize
        /// <summary>
        /// Combo Boxes - Method to select in range values on grid resize
        /// </summary>
        /// <param name="MaxComboIndexValue"></param>
        private void SetComboBoxInLimits(int? MaxComboIndexValue)
        {
            if (ComboBoxXValue.HasValue)
            {
                if (ComboBoxXValue > MaxComboIndexValue)
                {
                    ComboBoxXValue = MaxComboIndexValue;
                }
                if (ComboBoxYValue > MaxComboIndexValue)
                {
                    ComboBoxYValue = MaxComboIndexValue;
                }
            }
        }
        #endregion

        #region SetPositionToValid(int someInt) : Method to reset postion to valid board position
        /// <summary>
        /// Toy robot position - Method to reset 
        /// position if not in range of new board
        /// </summary>
        /// <param name="MaxComboIndexValue"></param>
        private void SetPositionToValid(int newSize)
        {
            if (ToyRobotSim != null)
            {
                if (ToyRobotSim.RobotPosition.X > newSize - 1)
                {
                    ToyRobotSim.RobotPosition.X = newSize - 1;
                }
                if (ToyRobotSim.RobotPosition.Y > newSize - 1)
                {
                    ToyRobotSim.RobotPosition.Y = newSize - 1;
                }
                SimulatorProxy.SetToyRobot(ToyRobotSim);
            }
        }
        #endregion

        #region Grid size: int property
        /// <summary>
        /// Toy Grid: properties to track and set grid size
        /// </summary>
        private int gridSize;
        public int GridSize
        {
            get { return gridSize; }
            set
            {
                if (value != gridSize)
                {
                    gridSize = value;
                    RaisePropertyChanged(() => GridSize);
                }
            }
        }
        #endregion

        #region Old Grid size: int property
        public int OldGridSize { get; set; }
        #endregion

        #region LanguageCombo: list property
        /// <summary>
        /// Language Combo Box: List property to store language cultures
        /// </summary>
        private ObservableCollection<WPFComboBoxItem> languageComboList;
        public ObservableCollection<WPFComboBoxItem> LanguageComboList
        {
            get { return languageComboList; }
            set
            {
                if (Equals(languageComboList, value)) return;
                languageComboList = value;
                RaisePropertyChanged(() => LanguageComboList);
            }
        }
        #endregion

        #region GetLanguageCombo: Method to create list
        public ObservableCollection<WPFComboBoxItem> GetLanguageComboList(ObservableCollection<CultureInfo> cultureList)
        {
            // get the cultures and extract the language info into the combo box items
            var thisComboBoxList = new ObservableCollection<WPFComboBoxItem>();
            foreach (CultureInfo culture in cultureList)
            {
                var thisItem = new WPFComboBoxItem();
                thisItem.ComboTextValue = culture.EnglishName;
                thisItem.ComboCultureValue = culture;
                thisComboBoxList.Add(thisItem);
            }
            return thisComboBoxList;
        }
        #endregion

        #region CoordCombo: list property
        public ObservableCollection<WPFComboBoxItem> CoordComboList { get; set; }
        #endregion

        #region GetCoordCombo: Method to create list
        public ObservableCollection<WPFComboBoxItem> GetCoordComboList()
        {
            var thisComboBoxList = new ObservableCollection<WPFComboBoxItem>();
            for (int i = 0; i < GridSize; i++)
            {
                var thisItem = new WPFComboBoxItem();
                thisItem.ComboValue = i;
                thisComboBoxList.Add(thisItem);
            }
            return thisComboBoxList;
        }
        #endregion

        #region ComboBoxXValue: int property
        private int? comboBoxXValue = null;
        public int? ComboBoxXValue
        {
            get { return comboBoxXValue; }
            set
            {
                if (value != comboBoxXValue)
                {
                    comboBoxXValue = value;
                    RaisePropertyChanged(() => ComboBoxXValue);
                }
            }
        }
        #endregion

        #region ComboBoxYValue: int property
        private int? comboBoxYValue = null;
        public int? ComboBoxYValue
        {
            get { return comboBoxYValue; }
            set
            {
                if (value != comboBoxYValue)
                {
                    comboBoxYValue = value;
                    RaisePropertyChanged(() => ComboBoxYValue);
                }
            }
        }
        #endregion

        #region LanguageComboValue: int property
        private int languageComboValue;
        public int LanguageComboValue
        {
            get { return languageComboValue; }
            set
            {
                if (value != languageComboValue)
                {
                    languageComboValue = value;
                    RaisePropertyChanged(() => LanguageComboValue);
                }
            }
        }
        #endregion

        #region LangSelected: CultureInfo property
        public CultureInfo LangSelected { get; set; }
        #endregion

        #region IsWindowClosed: Bool property for window behaviour
        private bool isWindowClosed;
        public bool IsWindowClosed
        {
            get { return isWindowClosed; }
            set
            {
                if (value != isWindowClosed)
                {
                    isWindowClosed = value;
                    RaisePropertyChanged(() => IsWindowClosed);
                }
            }
        }

        #endregion
        
        #region DoScrollToBottom: Bool property for text box behaviour
        private bool doScrollToBottom;
        public bool DoScrollToBottom
        {
            get { return doScrollToBottom; }
            set
            {
                if (value != doScrollToBottom)
                {
                    doScrollToBottom = value;
                    RaisePropertyChanged(() => DoScrollToBottom);
                }
            }
        }
        #endregion

        #region UI Labels: text properties
        private string changeGridSizeLabel;
        public string ChangeGridSizeLabel
        {
            get { return changeGridSizeLabel; }
            set
            {
                if (value != changeGridSizeLabel)
                {
                    changeGridSizeLabel = value;
                    RaisePropertyChanged(() => ChangeGridSizeLabel);
                }
            }
        }

        private string instructionsTitleLabel;
        public string InstructionsTitleLabel
        {
            get { return instructionsTitleLabel; }
            set
            {
                if (value != instructionsTitleLabel)
                {
                    instructionsTitleLabel = value;
                    RaisePropertyChanged(() => InstructionsTitleLabel);
                }
            }
        }

        private string instructionsTextLabel;
        public string InstructionsTextLabel
        {
            get { return instructionsTextLabel; }
            set
            {
                if (value != instructionsTextLabel)
                {
                    instructionsTextLabel = value;
                    RaisePropertyChanged(() => InstructionsTextLabel);
                }
            }
        }

        private string coordinatesLabel;
        public string CoordinatesLabel
        {
            get { return coordinatesLabel; }
            set
            {
                if (value != coordinatesLabel)
                {
                    coordinatesLabel = value;
                    RaisePropertyChanged(() => CoordinatesLabel);
                }
            }
        }

        private string directionLabel;
        public string DirectionLabel
        {
            get { return directionLabel; }
            set
            {
                if (value != directionLabel)
                {
                    directionLabel = value;
                    RaisePropertyChanged(() => DirectionLabel);
                }
            }
        }

        private string moveLabel;
        public string MoveLabel
        {
            get { return moveLabel; }
            set
            {
                if (value != moveLabel)
                {
                    moveLabel = value;
                    RaisePropertyChanged(() => MoveLabel);
                }
            }
        }

        private string reportLabel;
        public string ReportLabel
        {
            get { return reportLabel; }
            set
            {
                if (value != reportLabel)
                {
                    reportLabel = value;
                    RaisePropertyChanged(() => ReportLabel);
                }
            }
        }

        private string exitLabel;
        public string ExitLabel
        {
            get { return exitLabel; }
            set
            {
                if (value != exitLabel)
                {
                    exitLabel = value;
                    RaisePropertyChanged(() => ExitLabel);
                }
            }
        }

        private string placeLabel;
        public string PlaceLabel
        {
            get { return placeLabel; }
            set
            {
                if (value != placeLabel)
                {
                    placeLabel = value;
                    RaisePropertyChanged(() => PlaceLabel);
                }
            }
        }
        #endregion UI Labels: text properties
    }
}
