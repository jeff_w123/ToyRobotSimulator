﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using ToySimWCFService;

namespace WPFClient.ViewModel
{
    /// <summary>
    /// A base class to provide:
    /// 1. INPC
    /// 2. Callback for duplex service (abstract)
    /// 3. ServiceLocator members
    /// </summary>
    public abstract class VMbase : INotifyPropertyChanged, IContractCallback
    {
        /// <summary>
        /// PropertyChanged event definition needed for INotifyPropertyChanged interface
        /// </summary>
        #region PropertyChanged and RaisePropertyChanged declarations
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Create anonymous method to remove the property name magic strings
        /// </summary>
        /// <typeparam name="T">for any type of property</typeparam>
        /// <param name="propertyExpression">expression from which a property name can be drawn</param>
        protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs((propertyExpression.Body as MemberExpression).Member.Name));
        }
        #endregion
        
        /// <summary>
        /// Callback method to update the VM Toy Tobot property.
        /// </summary>
        #region Callback Method: RefreshRobot()
        public abstract void RefreshRobot();
        #endregion

        /// <summary>
        /// Service Locator: Property and Method
        /// </summary>
        #region Service Locator: Property and GetService<T>()
        ServiceLocator serviceLocator = new ServiceLocator();

        /// <summary>
        /// Gets the service locator 
        /// </summary>
        public ServiceLocator ServiceLocator
        {
            get
            {
                return serviceLocator;
            }
        }

        /// <summary>
        /// Gets a service from the service locator
        /// </summary>
        /// <typeparam name="T">The type of service to return</typeparam>
        /// <returns>Returns a service that was registered with the Type T</returns>
        public T GetService<T>()
        {
            return serviceLocator.GetService<T>();
        } 
        #endregion
    }
}
