﻿using System.Globalization;
namespace WPFClient.Utilities
{   
    /// <summary>
    /// A tiny class for use in combo box lists
    /// </summary>
    public class WPFComboBoxItem
    {
        public int ComboValue { get; set; }
        public string ComboTextValue { get; set; }
        public CultureInfo ComboCultureValue { get; set; }
    }
}
