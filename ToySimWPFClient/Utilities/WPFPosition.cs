﻿namespace WPFClient.Utilities
{
    /// <summary>
    /// Class to store the return value from GridBtnMultiBinding
    /// </summary>
    public class WPFPosition
    {
        public int GridX { get; set; }
        public int GridY { get; set; }

        public WPFPosition(int x, int y)
        {
            GridX = x;
            GridY = y;
        }
    }
}
