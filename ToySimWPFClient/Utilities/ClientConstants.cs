﻿namespace WPFClient
{
    public static class ClientConstants
    {
        //These values are required for unit tests!
        //
        //public const int MinGridSize = 3;
        //public const int MaxGridSize = 10;
        //public const int SliderDefaultValue = 5;

        public const int minGridSize = 3;
        public const int maxGridSize = 10;
        public const int sliderDefaultValue = 5;
    }
}
