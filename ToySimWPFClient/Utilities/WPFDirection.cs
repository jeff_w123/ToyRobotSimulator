﻿namespace WPFClient.Utilities
{
    /// <summary>
    /// This class is used in the MVVM as fields for storing direction button command params
    /// </summary>
    public enum WPFDirection
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }
}
