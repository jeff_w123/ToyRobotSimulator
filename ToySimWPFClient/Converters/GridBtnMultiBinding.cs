﻿using System;
using System.Globalization;
using System.Windows.Data;
using WPFClient.Utilities;
using WPFClient.View.Behaviours;

namespace WPFClient.Converters
{
    /// <summary>
    /// Method to convert the WPF grid position to a toy sim board position
    /// </summary>
    public class GridBtnMultiBinding : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var gridPositionX = int.Parse(values[0].ToString());
            var gridPositionY = int.Parse(values[1].ToString());
            return new WPFPosition(gridPositionX, gridPositionY);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        { throw new NotImplementedException(); }
    }
}
