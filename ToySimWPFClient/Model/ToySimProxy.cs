﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using ToySimWCFService;

namespace WPFClient.Model
{
    // WCF create proxy for ISimService using DuplexClientBase
    public class ToySimProxy : DuplexClientBase<ISimService>,
        ISimService
    {
        public ToySimProxy(object callbackInstance, Binding binding, EndpointAddress remoteAddress)
            : base(callbackInstance, binding, remoteAddress) { }

        public void RotateLeft()
        {
            Channel.RotateLeft();
        }

        public void RotateRight()
        {
            Channel.RotateRight();
        }

        public void Move()
        {
            Channel.Move();
        }

        public string ProcessCommand(string[] input)
        {
            return Channel.ProcessCommand(input);
        }

        public string GetReport()
        {
            return Channel.GetReport();
        }
        
        public void SetToyRobot(SVCToyRobot thisRobot)
        {
            Channel.SetToyRobot(thisRobot);
        }

        public SVCToyRobot GetToyRobot()
        {
           return Channel.GetToyRobot();
        }

        public void SetSquareBoardSize(int thisSize)
        {
            Channel.SetSquareBoardSize(thisSize);
        }

        public Boolean GetStatus()
        {
            return Channel.GetStatus();
        }

        public void SetIsRunningTrue()
        {
            Channel.SetIsRunningTrue();
        }

        public ObservableCollection<CultureInfo> GetAvailableCultures()
        {
            return Channel.GetAvailableCultures();
        }

        public void SetCulture(CultureInfo thisCulture)
        {
            Channel.SetCulture(thisCulture);
        }

        public System.Drawing.Bitmap GetFlagImage()
        {
            return Channel.GetFlagImage();
        }

        public string GetResourceText(string resourceName)
        {
            return Channel.GetResourceText(resourceName);
        }
    }
}
