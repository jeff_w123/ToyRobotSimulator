﻿using System;
using System.ServiceModel;
using ToySimWCFService;

namespace WPFClient.Model
{
    /// <summary>
    /// A lightweight proxy creator, called once for each instance of the WPF client.
    /// </summary>
    public static class ProxyFactory
    {
        public static ISimService CreateProxy(Object instanceContext, String endpointConfig)
        {
            //create context, channel, bindings and proxy
            return new DuplexChannelFactory<ISimService>
                (new InstanceContext(instanceContext), endpointConfig).CreateChannel();
        }
    }
}
