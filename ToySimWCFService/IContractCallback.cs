﻿using System.ServiceModel;

namespace ToySimWCFService
{
    public interface IContractCallback
    {
        [OperationContract(IsOneWay = true)]
        void RefreshRobot();
    }
}
