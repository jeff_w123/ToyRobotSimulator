﻿using System.ServiceModel;

namespace ToySimWCFService
{   
    // Service contract
    [ServiceContract]
    public interface ISimServiceNoCallback
    {
        [OperationContract]
        string ProcessCommand(string[] input);

        [OperationContract]
        bool GetStatus();

        [OperationContract]
        void SetIsRunningTrue();

        [OperationContract]
        string GetResourceText(string resourceName);
    }
}