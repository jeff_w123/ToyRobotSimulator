﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;
using Model.Factories;
using Model;
using Model.Toy;
using Model.ToyBoard;

namespace ToySimWCFService
{
    // create single instance behaviour for the service
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class SimService : ISimService, ISimServiceNoCallback
    {
        // create an instance of the toyrobot
        private ToySim thisSim = ToyFactory.CreateToySim(5, 5);

        public string ProcessCommand(string[] input)
        {
            try
            {
                var output = thisSim.ProcessCommand(input);
                return output;
            }
            // catch any arg ex and rethrow it as a wcf faultexception
            catch (ArgumentException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public string GetReport()
        {
            return thisSim.GetReport();
        }

        public void RotateLeft()
        {
            thisSim.ToyRobot.RotateLeft();
            Callback.RefreshRobot();
        }

        public void RotateRight()
        {
            thisSim.ToyRobot.RotateRight();
            Callback.RefreshRobot();
        }

        public void Move()
        {
            var newPosition = thisSim.ToyRobot.GetNextPosition();
            if (thisSim.SquareBoard.IsValidPosition(newPosition))
                thisSim.ToyRobot.Position = newPosition;
            Callback.RefreshRobot();
        }

        public SVCToyRobot GetToyRobot()
        {
            if (thisSim.ToyRobot.Position == null)
            {
                return null;
            }
            else
            {
                return new SVCToyRobot
                {
                    RobotDirection = (int)thisSim.ToyRobot.Direction,
                    RobotPosition = new SVCPosition(thisSim.ToyRobot.Position.X,
                        thisSim.ToyRobot.Position.Y)
                };
            }
        }

        public void SetToyRobot(SVCToyRobot thisRobot)
        {
            thisSim.ToyRobot.Position = new Position(thisRobot.RobotPosition.X,
                thisRobot.RobotPosition.Y);
            thisSim.ToyRobot.Direction = (Direction)thisRobot.RobotDirection;
            Callback.RefreshRobot();
        }

        public void SetSquareBoardSize(int newSize)
        {
            thisSim.SquareBoard = new ToyBoard(newSize, newSize);
        }

        public Boolean GetStatus()
        {
            return thisSim.IsRunning;
        }

        public void SetIsRunningTrue()
        {
            thisSim.IsRunning = true;
        }

        public ObservableCollection<CultureInfo> GetAvailableCultures()
        {
            return thisSim.SimLanguage.GetAvailableCultures();
        }

        public System.Drawing.Bitmap GetFlagImage()
        {
            return thisSim.GetFlagImage();
        }
        
        public void SetCulture(CultureInfo thisCulture)
        {
            thisSim.SimLanguage.SetCulture(thisCulture);
        }

        public string GetResourceText(string resourceName)
        {
            return thisSim.GetResourceText(resourceName);
        }

        private IContractCallback Callback
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IContractCallback>();
            }
        }
    }
}
