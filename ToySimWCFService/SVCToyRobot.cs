﻿using System.Runtime.Serialization;

namespace ToySimWCFService
{     
    // Data contract
    [DataContract(Namespace = "http://toyrobot.org")]
    public class SVCToyRobot
    {
        private SVCPosition robotPosition;
        private int? robotDirection;

        [DataMember]
        public SVCPosition RobotPosition
        {
            get 
            { 
                return robotPosition; 
            }
            set 
            { 
                robotPosition = value; 
            }
        }

        [DataMember]
        public int? RobotDirection
        {
            get 
            { 
                return robotDirection; 
            }
            set 
            { 
                robotDirection = value; 
            }
        }
    }
}