﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;

namespace ToySimWCFService
{     
    // Service contract
    [ServiceContract(Namespace = "http://toyrobot.org",
        CallbackContract = typeof(IContractCallback))]
    public interface ISimService
    {
        [OperationContract]
        string ProcessCommand(string[] input);

        [OperationContract]
        string GetReport();

        [OperationContract(IsOneWay = true)]
        void Move();

        [OperationContract(IsOneWay = true)]
        void RotateLeft();

        [OperationContract(IsOneWay = true)]
        void RotateRight();

        [OperationContract]
        SVCToyRobot GetToyRobot();

        [OperationContract(IsOneWay = true)]
        void SetToyRobot(SVCToyRobot thisRobot);

        [OperationContract(IsOneWay = true)]
        void SetSquareBoardSize(int newSize);

        [OperationContract]
        bool GetStatus();

        [OperationContract]
        void SetIsRunningTrue();

        [OperationContract]
        ObservableCollection<CultureInfo> GetAvailableCultures();

        [OperationContract]
        void SetCulture(CultureInfo thisCulture);

        [OperationContract]
        System.Drawing.Bitmap GetFlagImage();

        [OperationContract]
        string GetResourceText(string resourceName);
    }
}