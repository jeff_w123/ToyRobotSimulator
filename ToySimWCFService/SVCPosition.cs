﻿using System.Runtime.Serialization;

namespace ToySimWCFService
{     
    [DataContract(Namespace = "http://toyrobot.org")]
    public class SVCPosition
    {
        public SVCPosition(int a, int b)
        {
            x = a;
            y = b;
        }

        private int x;
        private int y;

        [DataMember]
        public int X
        {
            get 
            { 
                return x; 
            }
            set 
            { 
                x = value; 
            }
        }

        [DataMember]
        public int Y
        {
            get 
            { 
                return y; 
            }
            set 
            { 
                y = value; 
            }
        }
    }
}